import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { MockDataService } from './mock-data.service';
import { Observable } from 'rxjs/Observable';
import { Country } from '../models/country';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class CountryService {

  private subject = new BehaviorSubject([]);
  public countryList$: Observable<Country[]> = this.subject.asObservable();
  private API = {
    HOST: 'http://localhost:8080/api/pub',
    COUNTRY_LIST: '/countries'
  };

  constructor(private _http: Http, private mockDataService: MockDataService) {
    this.init();
  }

  public init() {
    return this.mockDataService.countriesAll()
      .subscribe((list) => this.subject.next(list));
  }

  public search(term$: Observable<string>) {
    return term$.combineLatest(this.countryList$,
      (term, countryList) => {
        const res = countryList.filter((i: Country) => {
          return term ? i.name.toUpperCase().indexOf(term.toUpperCase()) > -1 : true;
        });
        return res;
      });
  }

}
