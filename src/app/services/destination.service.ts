import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import {MockDataService} from "./mock-data.service";

@Injectable()
export class DestinationService {
  constructor(private _http: Http, private mockDataService:MockDataService) {
  }

  topDestionations() {
    let top = this.mockDataService.destinations();
    return top;
  }


}
