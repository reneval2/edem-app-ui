import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class OiRService {
  private url = 'http://maps.googleapis.com/maps/api/geocode/json?address=';

  constructor(private _http: Http) {
  }

  getMockData() {
    return this._http.get('/assets/mock-data/orel-i-reshka.json')
      .map((res) => res.json());
  }

}
