import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class MockDataService {

  private URI = {
    HOST: '/assets/mock-data/',
    DESTINATIONS: 'destinations.json',
    COUNTRIES: 'mock-countries.json',
    OiR: 'orel-i-reshka.json',
  };

  constructor(private _http: Http) {
  }

  public countriesAll() {
    return this._http.get(this.URI.HOST + this.URI.COUNTRIES)
      .map((res) => res.json());
  }

  public destinations() {
    return this._http.get(this.URI.HOST + this.URI.DESTINATIONS)
      .map((res) => res.json().top);
  }

  public oir() {
    return this._http.get(this.URI.HOST + this.URI.OiR)
      .map((res) => res.json().top);
  }
}
