import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class GoogleMapService {
  private url = 'http://maps.googleapis.com/maps/api/geocode/json?address=';

  constructor(private _http: Http) {
  }

  getCoordinates(destination: string) {
    return this._http.get(this.url + destination)
      .map(res => res.json());
  }

}
