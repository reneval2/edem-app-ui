import {Routes} from '@angular/router';
import {NoContentComponent} from './components/no-content';

import {
  AboutComponent,
  CountriesComponent,
  ProfileComponent,
  SearchComponent,
  LandingComponent,
  CountryItemComponent,
  SandboxComponent
} from './components/pages';

export const ROUTES: Routes = [
  {path: '', component: LandingComponent},
  {path: 'landing', component: LandingComponent},
  {path: 'countries', component: CountriesComponent},
  {path: 'country/:id', component: CountryItemComponent},
  {path: 'sandbox', component: SandboxComponent},
  {path: 'about', component: AboutComponent},
  {path: 'profile', component: ProfileComponent},
  {path: 'search', component: SearchComponent},
  {path: '**', component: NoContentComponent},
];
