import {Routes} from '@angular/router';
import {NoContentComponent} from '../components/no-content';

import {} from '../components/pages';
import {AdminComponent} from "./admin.component";
import {AdminProfileComponent} from "./profile/admin-profile.component";

export const ADMIN_ROUTES: Routes = [
  {
    path: 'admin',
    children: [
      { path: 'profile', component: AdminProfileComponent},
      { path: '', component: AdminComponent},
    ]
  }
];
