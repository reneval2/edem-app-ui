import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {Http, HttpModule} from '@angular/http';
import {
  NgModule,
} from '@angular/core';
import {AdminComponent} from "./admin.component";
import {RouterModule} from "@angular/router";
import {ADMIN_ROUTES} from "./admin.routes";
import {AdminProfileComponent} from "./profile/admin-profile.component";

@NgModule({

  declarations: [AdminComponent, AdminProfileComponent],

  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forChild(ADMIN_ROUTES),
  ],
  providers: []
})
export class AdminModule {

}
