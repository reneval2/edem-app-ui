import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class CountryService {
  private API = {
    HOST: 'http://localhost:8080/api/pub',
    COUNTRY_LIST: '/countries'
  };

  constructor(private _http: Http) {
  }

  public listAll() {
    return this._http.get(this.API.HOST + this.API.COUNTRY_LIST);
  }
}
