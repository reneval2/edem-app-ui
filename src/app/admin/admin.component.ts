import {
  Component,
  ViewEncapsulation, OnInit
} from '@angular/core';



@Component({
  selector: 'app',
  encapsulation: ViewEncapsulation.None,
  styleUrls: [
    './admin.component.css'
  ],
  templateUrl : './admin.component.html'
})
export class AdminComponent  implements OnInit {
  ngOnInit(): void {
    console.log("AdminComponent");
  }



}
