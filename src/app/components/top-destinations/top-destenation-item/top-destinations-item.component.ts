import {
  Component, Input,
  OnInit
} from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'edem-top-destinaetions-item',

  providers: [],
  styleUrls: ['./top-destinations-item.component.scss'],
  templateUrl: './top-destinations-item.html'
})
export class TopDestinationItemComponent {
  @Input() destination;

  constructor(private _router: Router) {
  }

  public navigate() {
    console.log(this.destination.id);
    this._router.navigate(['country', this.destination.id.toString()])
      .then(
        (err)=>{ console.log("ss:", err)},
        (err)=>{ console.log("err:", err)}
        );
  }
}
