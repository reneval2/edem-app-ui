import {
  Component,
  OnInit
} from '@angular/core';
import { DestinationService } from '../../services/destination.service';

@Component({
  selector: 'edem-top-destinations',

  providers: [],
  styleUrls: ['top-destinations.component.scss'],
  templateUrl: './top-destinations.html'
})
export class TopDestinationComponent {
  public topDestinations = [];

  constructor(private destinationService: DestinationService) {
    destinationService.topDestionations().subscribe((top) => this.topDestinations = top);
  }
}
