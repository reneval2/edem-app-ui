import {
  Component,
  OnInit
} from '@angular/core';
import {AppState} from "../../../app.service";



@Component({
  selector : 'landing',

  providers : [],
  styleUrls : ['./landing.component.css'],
  templateUrl : './landing.component.html'
})
export class LandingComponent implements OnInit {
  constructor(public appState: AppState) {
  }
  public ngOnInit() {
  }
}
