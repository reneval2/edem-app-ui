import {
  Component,
  OnInit,
  ElementRef
} from '@angular/core';
import {AppState} from "../../../app.service";

// EDEM
// const ITTOUR_URL = 'http://module.ittour.com.ua/tour_search.jsx?id=D66025G11532857376068N0&amp;ver=1&amp;type=2970';

//TEST ACC
const ITTOUR_URL = 'http://module.ittour.com.ua/tour_search.jsx?id=4239351D30G34O8356113619&ver=1&type=2970';

//Local
// const ITTOUR_URL = 'assets/js/ittour_test.js';


declare global {
  interface Window {
    load_css(): void;
  }
}

@Component({
  providers : [],
  styleUrls : ['./search.component.scss'],
  templateUrl : './search.component.html'
})

export class SearchComponent implements OnInit {
  myElement: ElementRef;

  constructor(public appState: AppState, myElement: ElementRef) {
    this.loadScript(ITTOUR_URL);
    this.myElement = myElement;
  }

  public ngOnInit() {
  }

  public test() {
    this.loadScript(ITTOUR_URL);
  }

  public loadScript(url) {
    let node = document.createElement('script');
    node.src = url;
    node.type = 'text/javascript';
    node.async = true;
    node.charset = 'windows-1251';
    node.onload = ()=> {
      window.load_css();
      let searchEl = this.myElement.nativeElement.querySelector('#isolate>table');
      if(searchEl) {searchEl.style.width = '100%';}
    };
    document.getElementsByTagName('head')[0].appendChild(node);
  }

  public onClickSearchModule(){
    let bannerEl = this.myElement.nativeElement.querySelector('#banner_container');
    if(bannerEl) {bannerEl.style.display = 'none!important';}
  }

}
