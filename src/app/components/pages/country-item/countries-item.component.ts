import {
  Component,
  OnInit
} from '@angular/core';


@Component({
  selector: 'countries',

  providers: [],
  styleUrls: ['./countries-item.component.css'],
  templateUrl: './countries-item.component.html'
})
export class CountryItemComponent implements OnInit {

  constructor() {
  }

  public ngOnInit() {
    console.log("Country item");
  }
}
