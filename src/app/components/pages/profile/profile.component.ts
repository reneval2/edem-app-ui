import {
  Component,
  OnInit
} from '@angular/core';
import {AppState} from "../../../app.service";


@Component({
  providers: [],
  styleUrls: [ './profile.component.css' ],
  templateUrl: './profile.component.html'
})
export class ProfileComponent implements OnInit {
  constructor(
    public appState: AppState,

  ) {}
  public ngOnInit() {
  }
}
