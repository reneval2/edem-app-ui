import {
    Component,
    OnInit
} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'about',
    templateUrl: 'about.component.html',
    styleUrls: ['about.component.scss']
})
export class AboutComponent {
    map = {
        lat: 50.441736,
        lng: 30.516135,
        zoom: 17,
        scrollwheel: false
    };


    contacts = {
        email: 'edem@bg.net.ua',
        phone: [
            "+380-66-388-03-58",
            "+380-93-731-43-43",
            "+380-44-234-48-08",
            "+380-044-234-11-91",
            "+380-044-234-63-60"
        ],
        skype: '',
        address: 'Edem tour<br />Pushkinskaya str. 32B<br />Kyiv, Ukraine<br/>',
        schedule: "Mon - Fri",
        scheduleTime: "10:00 a.m - 7:00p.m"
    };

    constructor(public route: ActivatedRoute) {
    }

}
