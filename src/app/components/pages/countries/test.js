function show() {
  var tbl = getElement('#mw-content-text > table:nth-child(19)');
  var res =[];
  var currentSeason;
  var seasonNum = 0;
  for (var i = 0, row; row = tbl.rows[i]; i++) {

    var season = isSeason(row);
    if (season) {
      currentSeason = season;
      seasonNum++;
    } else if (!isHeader(row)) {

      var data = pasrseRow(row);
      if(data) {
        data.season = currentSeason;
        data.seasonNum = seasonNum;
        res.push(data);
      }
      // console.log(data);
      // pasrseRow(row);
    }
  }
return res;
}


function getElement(str) {
  return document.querySelectorAll(str)[0];
}

function isSeason(row) {
  if (row.childNodes && row.childNodes.length == 3) {
    return row.childNodes[1].innerHTML;
  }
  return undefined;
}

function isHeader(row) {
  return row.getElementsByTagName('th') > 0;
}

function pasrseRow(row) {
  var re = /\((.*)\)/;

  var res = {
    episode: "",
    episodeSeason: "",
    episodeTotal: "",
    city: "",
    country: ""
  };

  if (row.childNodes.length < 2 || row.childNodes[1].length < 2) return;
  res.episode = row.cells[0].childNodes[0].innerHTML;
  if (!res.episode) return;
  res.episodeSeason = res.episode.split(' ')[0].trim();
  res.episodeTotal = res.episode.split(' ')[1].match(re)[1];
  res.city = row.cells[1].childNodes[2].innerHTML;
  res.country = row.cells[1].childNodes[0].childNodes[0].getAttribute("title");
  return res;
}

JSON.stringify(show());