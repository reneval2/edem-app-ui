import {
  Component,
  OnInit, AfterViewInit
} from '@angular/core';
import { CountryService } from '../../../services/country.service';
import {  Subject } from 'rxjs';
import { Country } from '../../../models/country';

@Component({
  selector: 'countries',
  providers: [],
  styleUrls: ['./countries.component.css'],
  templateUrl: './countries.component.html'
})
export class CountriesComponent implements AfterViewInit, OnInit {

  private countries: Country[];
  private search$ = new Subject<string>();

  constructor(private countryService: CountryService) {
    this.countryService.search(this.search$)
      .subscribe((list: Country[]) => this.countries = list);
    this.search$.next('');
  }

  public ngOnInit() {
    // this.sortedCountries = this.countryService.all();

  }

  public ngAfterViewInit() {
  }

}
