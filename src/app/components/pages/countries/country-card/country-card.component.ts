import {
  Component,
  OnInit, Input
} from '@angular/core';

import { Router } from "@angular/router";

@Component({
  selector: 'country-card',

  providers: [],
  styleUrls: ['country-card.component.css'],
  templateUrl: 'country-card.component.html'
})

export class CountryCardComponent {

  @Input() country;
  constructor(private _router: Router) {
  }

  navigate() {
    this._router.navigate(['/country',  this.country.id]);
  }

}
