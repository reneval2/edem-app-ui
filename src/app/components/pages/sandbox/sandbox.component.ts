import {
  Component,
  OnInit
} from '@angular/core';

import {
  GoogleMapService,
  OiRService,
  CountryService
} from '../../../services';
import { AppState } from '../../../app.service';
import { Observable } from 'rxjs/Rx';
import { Http } from '@angular/http';

@Component({
  selector: 'sandbox',
  providers: [],
  templateUrl: './sandbox.component.html'
})
export class SandboxComponent implements OnInit {
  public series: any = [];
  public countries: any = [];

  constructor(public oirService: OiRService,
              public countryService: CountryService,
              public googleMapService: GoogleMapService,
              public appState: AppState,
              private _http: Http) {

    this.appState.set('page', 'SandboxComponent');
  }

  public ngOnInit() {

    this.googleMapService
      .getCoordinates('Odessa')
      .subscribe((res) => {});

    Observable.zip(
      this.oirService.getMockData(),
      this.countryService.countryList$,
      (oir, countries) => {
        return {oir, countries};
      }
    ).subscribe((i) => {

      this.series = i.oir;
      this.countries = i.countries;

      this.series.forEach(
        (i) => {
          let found = this.countries.find(
            (_country) => {
              return _country.name == i.country;
            }
          );
          i.found = found;
        });
    });

    // this.oirService.getMockData().subscribe(res=>{
    //   this.series = res;
    //   console.log(res);
    // });
    //
    // this.countryService.all().subscribe(res=> {
    //   this.countries = res;
    //   console.log(res);
    // });
  }

  public onSelect(event) {
    console.log(event);
  }

  public onCountriesSave() {
    this.countryService.all()
      .subscribe((countries) => {
        Observable.from(countries)
        // .take(5)
          .subscribe((country) => {
            console.log(country);
            delete country.id;
            this._http.post('http://localhost:9000/api/countries', country)
              .subscribe((res) => {
                console.log(res);
              });
          });
      });

  }

}
