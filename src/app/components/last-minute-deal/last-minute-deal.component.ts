import {
    Component,
    OnInit
} from '@angular/core';


@Component({
    selector: 'edem-last-minute-deal',

    providers: [],
    styleUrls: ['./last-minute-deal.component.scss'],
    templateUrl: './last-minute-deal.component.html'
})
export class LastMinuteDealComponent implements OnInit {

    offer: any;

    constructor() {
    }

    public ngOnInit() {
        this.offer = {
            title: 'Last minute',
            dateFrom: new Date(2017, 7, 20),
            dateTo: new Date(2017, 7, 30),
            hotel: "Bellis Deluxe Hotel",
            region: "Belek - Antalya -Turkey",
            price: 1399,
            currency: 'USD',
            format: "per dbl room"
        }
    }

    public getPrice() {
        let res = "";
        if (this.offer.price) {
            res = '<b>' + this.getSumStringWithCurrency(this.offer) + '</b>';
            res += this.offer.format ? ' / ' + this.offer.format : '';
        }
        return res;
    }

    public getHotelName() {
        let res = this.offer.hotel || "";
        res += " " +  this.offer.region;
        return res;
    }

    getSumStringWithCurrency({price, currency}) {
        let res;
        if (!price || price <= 0) {
            res = "0";
        } else {
            switch (currency) {
                case "USD":
                    res = '$' + price;
                    break;
                case "EUR":
                    res = '€' + price;
                    break;
                default:
                    res = '' + price + currency;
            }
        }
        return res;
    }
}

