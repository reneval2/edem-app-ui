import {
  Component,
  AfterViewInit, style, state, animate, transition, trigger
} from '@angular/core';
import { HostListener } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

interface Window {
  scrollTop(): any;
}

@Component({
  selector: 'edem-navbar',
  providers: [],
  styleUrls: ['./navbar.component.scss'],
  templateUrl: './navbar.component.html',

  animations: [
    trigger('visibility', [
      state('shown', style({opacity: 1})),
      state('hidden', style({opacity: 0.4})),
      transition('* => *', animate('.2.5s'))
    ])
  ],
})
export class NavbarComponent implements AfterViewInit {

  public visibility;
  public menuItems = [];

  private scroll$ = new Subject;
  private visibilityEvent$: Observable<any>;

  @ViewChild('navbar') navbar;

  constructor(translate: TranslateService) {
    this.visibility = 'shown';
    this.visibilityEvent$ = new Observable<any>();

    translate.get('MENU')
      .subscribe( () => {
        this.menuItems = [
          {name: 'MENU.DESTINATIONS', route: './destinations', order: 1},
          {name: 'MENU.SEARCH', route: './search', order: 2},
          {name: 'MENU.COUNTRIES', route: './countries', order: 3},
          {name: 'MENU.ABOUT_US', route: './about', order: 6},
          {name: 'MENU.ADMIN', route: './admin', order: 8},
          {name: 'Sandbox', route: './sandbox', order: 99},
        ];
        }
      );

  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll(event) {
    //noinspection TypeScriptUnresolvedVariable
    this.scroll$.next(window.pageYOffset || 0);
  }

  ngAfterViewInit(): void {
    Observable.merge(
      Observable.fromEvent(this.navbar.nativeElement, 'mouseenter').map((a) => {
        return {mouseIn: true};
      }),
      Observable.fromEvent(this.navbar.nativeElement, 'mouseleave').map((a) => {
        return {mouseIn: false};
      }),
      this.scroll$.map((pos) => ({scroll: pos}))
    ).scan((acc, x) => {
      Object.assign(acc || {}, x);
      return acc;
    })
      .map((state: any) => {
        return (state.mouseIn || state.scroll == 0);
      })
      .debounceTime(300)
      .subscribe((e) => {
          this.visibility = e ? 'shown' : 'hidden';
        }
      );
  }
}
