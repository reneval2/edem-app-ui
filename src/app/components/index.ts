export * from './navbar/navbar.component';
export * from './footer/footer.component';
export * from './last-minute-deal/last-minute-deal.component';
export * from './country-carusel/country-carusel.component';
export * from './destination-carusel/destination-carusel.component';
export * from './top-destinations/top-destinations.component';
export * from './top-destinations/top-destenation-item/top-destinations-item.component';
