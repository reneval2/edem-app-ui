import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { Http, HttpModule } from '@angular/http';
import { AgmCoreModule } from '@agm/core';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import {
  NgModule,
  ApplicationRef
} from '@angular/core';
import {
  removeNgStyles,
  createNewHosts,
  createInputTransfer
} from '@angularclass/hmr';

import { RouterModule, PreloadAllModules } from '@angular/router';

import {
  AboutComponent,
  CountriesComponent,
  CountryItemComponent,
  ProfileComponent,
  SandboxComponent,
  LandingComponent,
  SearchComponent
} from './components/pages';

import { CountryService, OiRService, GoogleMapService } from './services';
import {
  FooterComponent,
  LastMinuteDealComponent,
  NavbarComponent,
  TopDestinationComponent,
  TopDestinationItemComponent,
  DestinationCaruselComponent,
  CountryCaruselComponent
} from './components';

/*
 * Platform and Environment providers/directives/pipes
 */
import { ENV_PROVIDERS } from './environment';
import { ROUTES } from './app.routes';
// App is our top level component
import { AppComponent } from './app.component';
import { APP_RESOLVER_PROVIDERS } from './app.resolver';
import { AppState, InternalStateType } from './app.service';

import { NoContentComponent } from './components/no-content';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DestinationService } from './services/destination.service';

import '../styles/styles.scss';
import { MockDataService } from "./services/mock-data.service";
import { CountryCardComponent } from "./components/pages/countries/country-card/country-card.component";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { NgxChartsModule } from "@swimlane/ngx-charts";
import {AdminModule} from "./admin/admin.module";

// Application wide providers
const APP_PROVIDERS = [
  ...APP_RESOLVER_PROVIDERS,
  AppState,
  OiRService,
  CountryService,
  GoogleMapService,
  DestinationService,
  MockDataService
];

type StoreType = {
  state: InternalStateType,
  restoreInputValues: () => void,
  disposeOldHosts: () => void
};

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: Http) {
  return new TranslateHttpLoader(http);
}

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [AppComponent],
  declarations: [
    AppComponent,
    AboutComponent,
    NoContentComponent,
    CountriesComponent,
    ProfileComponent,
    SearchComponent,
    SandboxComponent,
    LandingComponent,
    CountryItemComponent,

    NavbarComponent,
    FooterComponent,
    CountryCaruselComponent,
    LastMinuteDealComponent,
    TopDestinationComponent,
    DestinationCaruselComponent,
    TopDestinationItemComponent,
    CountryCardComponent

  ],
  /**
   * Import Angular's modules.
   */
  imports: [
    AdminModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    NgxChartsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [Http]
      }
    }),
    RouterModule.forRoot(ROUTES, {useHash: true, preloadingStrategy: PreloadAllModules}),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCzAwkKZ_aGaXg497iT3Lcls2FDl2b5m_M'
    })
  ],
  /**
   * Expose our Services and Providers into Angular's dependency injection.
   */
  providers: [
    ENV_PROVIDERS,
    APP_PROVIDERS
  ]
})
export class AppModule {

  constructor(public appRef: ApplicationRef,
              public appState: AppState,
              translate: TranslateService) {

    translate.addLangs(['ru', 'ua', 'en']);
    translate.setDefaultLang('en');
  }

  public hmrOnInit(store: StoreType) {
    if (!store || !store.state) {
      return;
    }
    console.log('HMR store', JSON.stringify(store, null, 2));
    /**
     * Set state
     */
    this.appState._state = store.state;
    /**
     * Set input values
     */
    if ('restoreInputValues' in store) {
      let restoreInputValues = store.restoreInputValues;
      setTimeout(restoreInputValues);
    }

    this.appRef.tick();
    delete store.state;
    delete store.restoreInputValues;
  }

  public hmrOnDestroy(store: StoreType) {
    const cmpLocation = this.appRef.components.map((cmp) => cmp.location.nativeElement);
    /**
     * Save state
     */
    const state = this.appState._state;
    store.state = state;
    /**
     * Recreate root elements
     */
    store.disposeOldHosts = createNewHosts(cmpLocation);
    /**
     * Save input values
     */
    store.restoreInputValues = createInputTransfer();
    /**
     * Remove styles
     */
    removeNgStyles();
  }

  public hmrAfterDestroy(store: StoreType) {
    /**
     * Display new elements
     */
    store.disposeOldHosts();
    delete store.disposeOldHosts;
  }

}
