var file_version = 55;

if(document.getElementById('tour_search_module') != null && typeof(document.getElementById('tour_search_module')) != 'undefined') {
  document.getElementById('tour_search_module').style.display = 'none';
}
//preload background
heavyImage = new Image();
heavyImage.src = '';

function load_stylesheet(filename) {
  var css = document.createElement('link');
  css.rel = 'stylesheet';
  css.type = 'text/css';
  css.href = filename;
  document.body.appendChild(css);
}

var script;
var is_script_load = new Array();
function load_script(filename, after_load) {
  is_script_load[filename] = false;
  script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = filename;
  document.getElementsByTagName('head')[0].appendChild(script);

  script.onreadystatechange = function () {
    if (this.readyState == 'loaded' || this.readyState == 'complete') {
      if(is_script_load[filename] == false) {
        is_script_load[filename] = true;
        load_js(after_load);
      }
    }
  }

  script.onload = function() {
    if(is_script_load[filename] == false) {
      is_script_load[filename] = true;
      load_js(after_load);
    }
  }
}

function load_js(type) {
  if(type == 'jquery')
    load_script("https://www.ittour.com.ua/classes/handlers/ittour_external_modules/ittour_modules/js/jquery-1.7.1.min.js?i=1", 'ui');
  if(type == 'ui')
    load_script("https://www.ittour.com.ua/classes/handlers/ittour_external_modules/ittour_modules/js/jquery-ui-1.8.6.custom.min.js?i=1", 'boxy');
  if(type == 'boxy')
    load_script("https://www.ittour.com.ua/classes/handlers/ittour_external_modules/ittour_modules/js/jquery.boxy.js?i=4", 'datepicker');
  if(type == 'datepicker')
    load_script("https://www.ittour.com.ua/classes/handlers/ittour_external_modules/ittour_modules/js/jquery.ui.datepicker-ru_utf-8.js?i=1", 'orbit');
  if(type == 'orbit')
    load_script("https://www.ittour.com.ua/classes/handlers/ittour_external_modules/ittour_modules/js/jquery.orbit-1.2.3.min.js?i=" + file_version, 'tour_seach_form');
  if(type == 'tour_seach_form')
    load_script("https://www.ittour.com.ua/classes/handlers/ittour_external_modules/ittour_modules/js/tour_seach_form.js?i=" + file_version, 'prepare_form');
  if(type == 'prepare_form'){
    //load custom css
    var ss1 = document.createElement('style');
    var custom_css = '/*Arial            - шрифт12       - размер основного шрифта#000000      - цвет основного шрифта - размер шрифта заголовка#FF0066     - цвет заголовка#602063    - цвет кнопки#DFB6E1  - цвет неактивной закладки#DFB6E1      - цвет фона#FFFFFF    - цвет рамки#FF0066      - цвет ссылок*//*===================font====================*//* change the font family*/html body #tour_search_module * ,html body #isolate {  color: #000000 !important;  font-size: 12px !important;  font-family: Arial !important;}html body #isolate * .itt_main_block,html body #tour_search_module * .itt_main_block *,html body #isolate * .itt_main_block * {  font-family: Arial !important;}html body #tour_search_module * .itt_main_block table,html body #isolate * .itt_main_block table{  font-family: Arial !important;}.tour_order_table{  font-family: Arial !important;}/* change the font size*/html body #tour_search_module * .itt_main_block *,html body #tour_search_module * .itt_main_block,html body #isolate * .itt_main_block{  font-size: 12px !important;}html body #isolate * .package_search_result_table .itt_results td{ font-size: 12px !important;}html body #isolate * .itt_hike_search_result_table tr td{ font-size: 12px !important;}html body #isolate * .hike_search_result_table .itt_crumbs ul li, html body #isolate * .package_search_result_table .itt_crumbs ul li{ font-size: 12px !important;}.tour_order_table{  font-size: 12px !important;}html body #isolate * ul,html body #isolate * ul{font-size: 12px !important;}/* change the font color*/html body #tour_search_module * .itt_main_block,html body #isolate * .itt_main_block * {  color: #000000 !important;}.tour_order_table{  /*color: #000000 !important;*/}html body #isolate * .itt_main_block input{  font-size: 12px !important;}.tour_order_table td{   font-size: 12px !important;}html body #tour_search_module * ul,html body #tour_search_module  .itt_main_block .btn-search input,html body #isolate * ul{color: #000000 !important;}html body #isolate * .itt_main_block input{	color: #000000 !important;}html body #isolate *.itt_main_block select{	color: #000000 !important;}/*===================end font====================*//*===================title====================*//* change the font-size of h1*/html body #isolate * .extended_package_search_form .frame_block h1.title {  font-size: px !important;}html body #isolate * .extended_hike_search_form .frame_block h1.title {  font-size: px !important;}html body #isolate * .package_search_result_table .itt_title h2.itt_result_title{ font-size: px !important;}html body #isolate * .hike_search_result_table .itt_title h2.itt_result_title{  font-size: px !important;}.tour_order_table h3{ font-size: px !important;}.tour_order_table h4{ font-size: px !important;}/* change the color h1*/html body #isolate * .extended_package_search_form .frame_block h1.title {  color: #FF0066 !important;}html body #isolate * .extended_hike_search_form .frame_block h1.title {  color: #FF0066 !important;}.tour_order_table h3{  /*color: #FF0066 !important;*/}.tour_order_table h4{  /*color: #FF0066 !important;*/}/*===================end title====================*//*===================button====================*//* change the color of the button*/html body #isolate * .extended_hike_search_form .itt_content .pager-sub .btn-search input{  background-color: #602063 !important;}html body #isolate * .extended_package_search_form .itt_content .pager-sub .btn-search input{  background-color: #602063 !important;}.package_order_form .send_block input{  background-color: #602063 !important;}.package_order_form .send_block input{  background-color: #602063 !important;}/*===================end button====================*//*===================tab====================*//* change the color of the link background*/html body #isolate * .extended_package_search_form .itt_links .no_active{  background: #DFB6E1 !important;}html body #isolate * .extended_hike_search_form .itt_links .no_active {  background: #DFB6E1 !important;}/*===================end tab====================*//*===================background====================*//* change the color of the main background*/html body #isolate * .extended_package_search_form .itt_links .active{  background: #DFB6E1 !important;}html body #isolate * .extended_hike_search_form .itt_links .active {  background: #DFB6E1 !important;}html body #isolate * .extended_hike_search_form .itt_content .first_box {  background: #DFB6E1 !important;}html body #isolate * .extended_hike_search_form .itt_content .second_box {  background: #DFB6E1 !important;}html body #isolate * .extended_package_search_form .itt_content .first_box {  background: #DFB6E1 !important;}html body #isolate * .extended_package_search_form .itt_content .second_box {  background: #DFB6E1 !important;}html body #isolate * .package_search_result_table .itt_results th.main_bg {  background: #DFB6E1 !important;}html body #isolate * .package_search_result_table .itt_results th.left_bg {  background: #DFB6E1 !important;}html body #isolate * .package_search_result_table .itt_results th.right_bg {  background: #DFB6E1 !important;}html body #isolate * .hike_search_result_table  th.main_bg {  background: #DFB6E1 !important;}html body #isolate * .hike_search_result_table  th.left_bg {  background: #DFB6E1 !important;}html body #isolate * .hike_search_result_table  th.right_bg {  background: #DFB6E1 !important;}html body #isolate * .package_search_result_table .itt_results tr.itt_even {  background: #DFB6E1 !important;}html body #isolate * .package_search_result_table .itt_results tr.odd {  background: #DFB6E1 !important;}/*html body #isolate * .package_search_result_table .itt_results tr{  background: #DFB6E1 !important;}html body #isolate * .itt_hike_search_result_table tr th {  background: #DFB6E1 !important;}*/html body #isolate * .itt_hike_search_result_table tr.even td {  background: #DFB6E1 !important;}html body #isolate * .itt_hike_search_result_table tr.odd td {  background: #DFB6E1 !important;}html body #isolate * .itt_hike_search_result_table tr td {  background: #DFB6E1 !important;}html body #isolate * .itt_hike_search_result_table tr td {  background: #DFB6E1 !important;}.tour_order{  /*background: #DFB6E1 !important;*/}html body #isolate * .extended_package_search_form .frame_block .itt_main_background {    background: none !important;}html body #isolate * .extended_hike_search_form .frame_block .itt_main_background {    background: none !important;}/*===================end background====================*//*===================border====================*//* change the color of the border*/html body #isolate * .extended_package_search_form .itt_links {  border-color: #FFFFFF !important;}html body #isolate * .extended_package_search_form .itt_links .active {  border-bottom-color: #DFB6E1 !important;}html body #isolate * .extended_package_search_form .itt_links .no_active{  border-bottom-color: #FFFFFF !important;}html body #isolate * .extended_hike_search_form .itt_links .no_active{  border-bottom-color: #FFFFFF !important;}html body #isolate * .extended_package_search_form .itt_links .package_tour {  border-top-color: #FFFFFF !important;  border-right-color: #FFFFFF !important;  border-left-color: #FFFFFF !important;}html body #isolate * .extended_package_search_form .itt_content {  border-bottom-color: #FFFFFF !important;  border-top-color:    #FFFFFF !important;}html body #isolate * .extended_package_search_form .itt_content .first_box {  border-left-color:  #FFFFFF !important;  border-right-color: #FFFFFF !important;  border-bottom-color: #FFFFFF !important;}html body #isolate * .extended_package_search_form .itt_content .second_box {  border-left-color:  #FFFFFF !important;  border-right-color: #FFFFFF !important;  border-top-color: #FFFFFF !important;}html body #isolate * .extended_hike_search_form .itt_links {  border-bottom-color: #FFFFFF !important;}html body #isolate * .extended_hike_search_form .itt_links .active {  border-bottom-color: #DFB6E1 !important;}html body #isolate * .extended_hike_search_form .itt_links .hike_tour {  border-top-color: #FFFFFF !important;  border-right-color: #FFFFFF !important;  border-left-color: #FFFFFF !important;}html body #isolate * .extended_hike_search_form .itt_content .first_box {  border-left-color:  #FFFFFF !important;  border-right-color: #FFFFFF !important;  border-bottom-color: #FFFFFF !important;}html body #isolate * .extended_hike_search_form .itt_content .second_box {  border-left-color:  #FFFFFF !important;  border-right-color: #FFFFFF !important;  border-top-color: #FFFFFF !important;}html body #isolate * .extended_hike_search_form .itt_content {  border-bottom-color: #FFFFFF !important;}html body #isolate * .package_search_result_table .itt_results th {  border-bottom-color: #FFFFFF !important;  border-top-color:    #FFFFFF !important;  border-bottom-width: 1px !important;  border-top-width:    1px !important;}html body #isolate * .package_search_result_table .itt_results th.with_right_border {  border-right-color:  #FFFFFF !important;  border-right-width:  1px !important;}html body #isolate * .package_search_result_table .itt_results {  border-bottom-color: #FFFFFF !important;  border-left-color:   #FFFFFF !important;  border-bottom-width: 1px !important;  border-left-width:   1px !important;}html body #isolate * .package_search_result_table .itt_results td.last {  border-right-color: #FFFFFF !important;}html body #isolate * .itt_hike_search_result_table {  border-bottom-color: #FFFFFF !important;  border-left-color:   #FFFFFF !important;  border-bottom-width: 1px !important;  border-left-width:  1px !important;  }html body #isolate * .itt_hike_search_result_table tr th {  border-bottom-color: #FFFFFF !important;  border-top-color:    #FFFFFF !important;  border-bottom-width: 1px !important;  border-top-width:    1px !important;}html body #isolate * .itt_hike_search_result_table tr th.with_right_border {  border-right-color:  #FFFFFF !important;  border-right-width:  1px !important;}html body #isolate * .itt_hike_search_result_table tr td.first {  border-left-color: #FFFFFF !important;}html body #isolate * .itt_hike_search_result_table tr td.last {  border-right-color: #FFFFFF !important;}html body #isolate * .itt_hike_search_result_table tr th.last {  border-right-color: #FFFFFF !important;}/*===================end border====================*//*===================link_color====================*/html body #isolate * .itt_hike_search_result_table tr td a *,html body #isolate * .itt_hike_search_result_table tr td a{  color: #FF0066 !important;}html body #isolate * .package_search_result_table tr td a *,html body #isolate * .package_search_result_table tr td a{  color: #FF0066 !important;}.tour_order_table .tour_general_info_border td a{  /*color: #FF0066 !important;*/}html body #isolate * .tour_general_info_no_border td a{  color: #FF0066 !important;}.tour_order_table .tour_general_info_border td a{  /*color: #FF0066 !important;*/} /*===================end link color====================*/html body #isolate * .package_search_result_table .itt_title h2.itt_result_title{border-bottom: 1px solid #FFFFFF !important;}html body #isolate * .package_search_result_table tr.itt_pager td.pager-content .pager-list ul li.active a{background:#DFB6E1 !important;}html body #isolate * .hike_search_result_table .itt_title h2.itt_result_title{border-bottom: 1px solid #FFFFFF !important;}html body #isolate * .hike_search_result_table tr.itt_pager td.pager-content .pager-list ul li.active a{background:#DFB6E1 !important;}html body #isolate * .extended_package_search_form .itt_content .pager-sub .logo_ittour a {  background: url("https://www.ittour.com.ua/images/itt_net_admin/1/module_logo/ittour_module_logo.png") no-repeat scroll 0 0 transparent  !important;}html body #isolate * .extended_hike_search_form .itt_content .pager-sub .logo_ittour a {  background: url("https://www.ittour.com.ua/images/itt_net_admin/1/module_logo/ittour_module_logo.png") no-repeat scroll 0 0 transparent  !important;}html body #isolate * .cart_link span{ color: #FF0066 !important;}/*inpur color reset*/html body #tour_search_module .itt_main_block input,html body #tour_search_module .itt_main_block select, html body #tour_search_module .itt_main_block option, html body #tour_search_module .itt_main_block textarea {    color: #000!important;}html body #isolate * .ittour_order_block_title_box_left_corner                              /*,html body #showcase_module * .it_bg_left_top*/                              {                                background: url("https://www.ittour.com.ua/classes/handlers/ittour_external_modules/ittour_modules/images/bg_cache/bg_top_module_left_DFB6E1_FFFFFF_DFB6E1__transparent.png") no-repeat 0 0!important;                              }                              html body #isolate * .ittour_order_block_title_box_info .ittour_order_block_title_box_left_corner{                                background: url("https://www.ittour.com.ua/classes/handlers/ittour_external_modules/ittour_modules/images/bg_cache/bg_top_module_left_DFB6E1_FFFFFF_DFB6E1__transparent.png") no-repeat 0 0!important;                              }                              html body #isolate * .it_bg_left_bottom_it                             /* ,html body #showcase_module * .it_bg_left_bottom*/                             {                                background: url("https://www.ittour.com.ua/classes/handlers/ittour_external_modules/ittour_modules/images/bg_cache/bg_bottom_module_left_DFB6E1_FFFFFF_DFB6E1__transparent.png") no-repeat 0 0!important;                              }                              html body #isolate * .ittour_order_block_title_box_center_corner_next,                              /* html body #showcase_module * .ittour_order_block_title_box_center_corner,*/                              html body #isolate * .ittour_order_block_title_box_center_corner                             /* ,html body #showcase_module * .it_bg_right_top*/{                                background: url("https://www.ittour.com.ua/classes/handlers/ittour_external_modules/ittour_modules/images/bg_cache/bg_top_module_right_DFB6E1_FFFFFF_DFB6E1__transparent.png") no-repeat right 0!important;                              }                              html body #isolate * .it_bg_right_bottom_it                              /*,html body #showcase_module * .it_bg_right_bottom */                              {                                background: url("https://www.ittour.com.ua/classes/handlers/ittour_external_modules/ittour_modules/images/bg_cache/bg_bottom_module_right_DFB6E1_FFFFFF_DFB6E1__transparent.png") no-repeat right 0!important;                              }                              html body #isolate * .ittour_order_block_title_box_info .ittour_order_block_title_box_center_corner_next{                                background: url("https://www.ittour.com.ua/classes/handlers/ittour_external_modules/ittour_modules/images/bg_cache/bg_top_module_right_DFB6E1_FFFFFF_DFB6E1__transparent.png") no-repeat right  0!important;                              }                              #isolated .tour_search_result,                              #tour_order,                              #isolate .message {                                border: 1px solid #FFFFFF!important;                              }                              html body #isolate * .it_bg_bottom_box{                                 border-bottom: 1px solid #FFFFFF!important;                              }                              html body #isolate * .ittour_order_block_content_box{                                border-top: 1px solid #FFFFFF!important;                                border-left: 1px solid #FFFFFF!important;                                border-right: 1px solid #FFFFFF!important;                              }                              html body #isolate * .it_gradient_right h2,                              html body #isolate * .it_gradient_right a  {                                color: #000000!important;                              }                              html body #isolate .it_gradient_right * {                                font: 13px Arial!important;                              }                              html body #tour_search_module * .itt_main_block * .it_small_price {                                font-size: 10px !important;                              }                              html body #tour_search_module * .itt_main_block * .it_big_font_size {                                font-size: 14px !important;                              }                              #isolate .package_search_result_table .itt_price_area .itt_price_area_row .itt_price_more{                                border-bottom: 1px dotted #FF0000!important;                              }                              #isolate .package_search_result_table .itt_price_line .itt_price{                                border-bottom: 2px dotted #FF0000!important;                              }                              #isolate .package_search_result_table .itt_price_line .itt_price,                               #isolate .package_search_result_table .itt_price_line .ttt_tour_price_currency,                               #isolate .package_search_result_table .itt_price_line,                               #isolate .package_search_result_table .itt_price_area .itt_price_area_row .itt_price_more,                               #isolate .package_search_result_table .itt_results_info h3{                                 color: #FF0000!important;                              }                              #isolate .package_search_result_table .itt_price_area .itt_add_to_basket span{                                  border-bottom:1px dotted #FF0066!important;                                  color: #FF0066!important;                              }                              #isolate .package_search_result_table .title-row h2 span,                              #isolate .package_search_result_table .itt_price_area .itt_add_to_basket,                               #isolate .package_search_result_table .title-row h2 a{                                color: #FF0066!important;                              }                               #isolate .package_search_result_table .itt_price_line span,                               #isolate .package_search_result_table .title-row h2 span,                               #isolate .package_search_result_table .title-row h2 a{                                font-size: 16px !important;                              }                              #isolate .package_search_result_table .itt_price_line .itt_price{                                font-size: 26px !important;                                line-height:  26px !important;                              }                              #isolate .package_search_result_table .itt_price_line .ttt_tour_price_currency{                                font-size: 22px !important;                                line-height:  22px !important;                              }                              #isolate .package_search_result_table .title-row h2{                                margin-top: 9px !important;                              }                              #isolate .extended_package_search_form .itt_content,                              #isolate .extended_hike_search_form .itt_content{                                  border: none;                              }                              #isolate .extended_package_search_form .itt_links,                              #isolate .extended_hike_search_form .itt_links{                                border-bottom: 1px solid [background-color] !important;                              }                              #isolate .extended_package_search_form .itt_links .package_tour,                              #isolate .extended_hike_search_form .itt_links .package_tour{                                border: 1px solid #FFFFFF;                                border-bottom: 1px solid !important;                              }                              #isolate .extended_package_search_form .itt_links .hike_tour,                              #isolate .extended_hike_search_form .itt_links .hike_tour{                                border: 1px solid #FFFFFF !important;                                border-bottom: 1px solid !important;                              }                              #isolate .extended_package_search_form .itt_links .active,                              #isolate .extended_hike_search_form .itt_links .active{                                border-bottom: 1px solid #DFB6E1 !important;                                position: relative;                              }                              #isolate .extended_package_search_form .itt_links .no_active,                              #isolate .extended_hike_search_form .itt_links .no_active{                                border: 1px solid #FFFFFF !important;                              }                              #isolate .extended_package_search_form .itt_content .first_box,                              #isolate .extended_hike_search_form .itt_content .first_box{                                border-bottom: none;                                border-top: 1px solid #FFFFFF !important;                                border-left: 1px solid #FFFFFF;                                border-right: 1px solid #FFFFFF;                              }                              #isolate .extended_package_search_form .itt_content .second_box,                              #isolate .extended_hike_search_form .itt_content .second_box{                                border-top: none;                                border-left: 1px solid #FFFFFF;                                border-right: 1px solid #FFFFFF;                                border-bottom: 1px solid #FFFFFF;                              }                               html body #isolate * .package_search_result_table .itt_results tr.itt_odd td{                                 background-color: #E8CBEA !important;                               }                               html body #isolate * .package_search_result_table .itt_results tr.itt_even td{                                  background-color: #DFB6E1 !important;                                }html body #tour_search_module * .extended_package_search_form .frame_block .itt_main_background /*fix bg in not 500x170*/ {border: 0px solid #FFFFFF !important;} tr.itt_odd td, .itt_hike_search_result_table tr.itt_odd td{ background: #FFFFFF !important;} html body #isolate * .tour_search_result .package_search_result_table .itt_results tr.itt_even{ background: #FFFFFF !important;}';
    ss1.setAttribute("type", "text/css");
    if (ss1.styleSheet) {   // IE
      ss1.styleSheet.cssText = custom_css;
    } else {                // the world
      var tt1 = document.createTextNode(custom_css);
      ss1.appendChild(tt1);
    }
    document.getElementsByTagName('head')[0].appendChild(ss1);
    // -------------
    jq('body').data('options', {'modules_url'          : 'https://www.ittour.com.ua/classes/handlers/ittour_external_modules/ittour_modules/'
      , 'modules_action'       : 'https://www.ittour.com.ua/tour_search.php?callback=?&module_type=tour_search&id=4239351D30G34O8356113619&ver=1&type=2970&theme=38'
      , 'modules_param_action' : 'https://www.ittour.com.ua/search_param.php?callback=?&module_type=tour_search&id=4239351D30G34O8356113619&ver=1&type=2970&theme=38'
      , 'modules_popup_result' : ''
      , 'modules_popup_type'   : 'div'
      , 'show_basket'          : '0'
      , 'use_hotel_result'     : '0'
      , 'preview'              : '0'
      , 'extended_search_url'  : 'http://mix-tour.com.ua/'
      , 'agency_id'            : '13518'
    });
    prepare_form();
  }
}

function start_js(no_confloct) {
  jQuery = no_confloct;
  load_js('tour_seach_form');
}

function load_css() {
  load_stylesheet("https://www.ittour.com.ua/classes/handlers/ittour_external_modules/ittour_modules/css/clear_all.css?i=" + file_version);
  load_stylesheet("https://www.ittour.com.ua/classes/handlers/ittour_external_modules/ittour_modules/css/tour_search_main_clr.css?i=" + file_version);
  load_stylesheet("https://www.ittour.com.ua/classes/handlers/ittour_external_modules/ittour_modules/css/tour_seach_form_clr_650x375.css?i=" + file_version);
  load_stylesheet("https://www.ittour.com.ua/classes/handlers/ittour_external_modules/ittour_modules/css/jquery-ui-1.7.2.custom.css?i=1");
  load_stylesheet("https://www.ittour.com.ua/classes/handlers/ittour_external_modules/ittour_modules/css/orbit-1.2.3.css?i=" + file_version);
  load_js('jquery');
}

function addLoadEvent(func) {
  if (document.addEventListener) {
    document.addEventListener("DOMContentLoaded", func, false);
  } else {
    var oldonload = window.onload;
    if (typeof window.onload != 'function') {
      window.onload = func;
    } else {
      window.onload = function() {
        if (oldonload) {
          oldonload();
        }
        func();
      }
    }
  }
}

function itt_is_bot() {
  var agent = navigator.userAgent;
  agent = agent.toLowerCase();

  if(agent.indexOf('google') != -1 || agent.indexOf('googlebot') != -1) {
    return 'google';
  }
  if(agent.indexOf('yandex') != -1) {
    return 'yandex';
  }
  if(agent.indexOf('stackrambler') != -1) {
    return 'rambler';
  }
  if(agent.indexOf('yahoo') != -1) {
    return 'yahoo';
  }
  if(agent.indexOf('msnbot') != -1) {
    return 'msnbot';
  }
  return false;
}

if(!itt_is_bot()) {
  addLoadEvent(load_css);
}

var tour_search_module = '';
tour_search_module += '<div id="isolate">';
tour_search_module += '<table width="100%" cellspacing="0" cellpadding="3" border="0" style="margin: 0pt 0pt 3px;/*table-layout:fixed;*/ border-collapse: collapse;">';
tour_search_module += '<tbody>';
tour_search_module += '<tr style="display:inline-table;width:100%;margin-bottom:5px;"  valign="top" >';
tour_search_module += '<td width="90%" align="center">';
tour_search_module += '<div class="itt_in_middel" style="margin:0 auto;width:650px;">';
tour_search_module += '<div class="itt_main_block">';
tour_search_module += '<input type="hidden" id="module_search_query" value="package">';
tour_search_module += '<div class="extended_package_search_form" >';
tour_search_module += '<div class="frame_block">';
tour_search_module += '<h1 class="title" style="display:none;" >Поиск тура</h1>';
tour_search_module += '<form method="get" id="package_search_form"  name="package_search_form" action=\'package_seach_result.php\'>';
tour_search_module += '<div class="itt_main_background">';
tour_search_module += '<input type="hidden" name=\'action\' value=\'package_tour_search\' />';
tour_search_module += '<input type=\'hidden\' id=\'hotel_rating\' name=\'hotel_rating\' />';
tour_search_module += '<input name="items_per_page" value="50" id="items_per_page50" checked="checked" type="hidden" />';
tour_search_module += '<input type=\'hidden\' id=\'hotel\' name=\'hotel\' />';
tour_search_module += '<input type=\'hidden\' id=\'region\' name=\'region\' />';
tour_search_module += '<input type=\'hidden\' id=\'child_age\' name=\'child_age\' />';
tour_search_module += '<div class="itt_links">';
tour_search_module += '<div class=\'package_tour active\'>Пакетные</div>';
tour_search_module += '<div class=\'hike_tour no_active\'><a href="javascript;" onclick="return display_hike_form();">Экскурсионные</a></div>';
tour_search_module += '<div class=\'check_option_tour\'>';
tour_search_module += '<input type="radio" value="1" name="package_tour_type" id="package_tour_type_1" onclick="return change_package_tour_type(this.value);"';
tour_search_module += 'checked="checked"><label for="package_tour_type_1">Проезд включен</label>';
tour_search_module += '<input type="radio" value="2" name="package_tour_type" id="package_tour_type_2" onclick="return change_package_tour_type(this.value);"><label';
tour_search_module += 'for="package_tour_type_2">Проезд не включен</label>';
tour_search_module += '</div>';
tour_search_module += '</div>';
tour_search_module += '<div class="itt_content">';
tour_search_module += '<div class="first_box">';
tour_search_module += '<div class="col-direction">';
tour_search_module += '<div class="country">';
tour_search_module += '<ul>';
tour_search_module += '<li>';
tour_search_module += '<label>Транспорт:</label>';
tour_search_module += '<select class="big" id="transport_type" name="transport_type">';
tour_search_module += '<option value=\'0\'>Все</option>';
tour_search_module += '<option value=\'1\' selected=\'selected\'>Авиа</option>';
tour_search_module += '<option value=\'2\' >Автобус</option>';
tour_search_module += '</select>';
tour_search_module += '</li>';
tour_search_module += '<li>';
tour_search_module += '<label>Страна:</label>';
tour_search_module += '<select class="big" id="itt_country" name="country" >';
tour_search_module += '<option value=\'30\'  >Австрия</option><option value=\'31\'  >Азербайджан</option><option value=\'23\'  >Албания</option><option value=\'24\'  >Андорра</option><option value=\'27\'  >Армения</option><option value=\'2313\'  >Аруба</option><option value=\'2037\'  >Багамские Острова</option><option value=\'1084\'  >Барбадос</option><option value=\'406\'  >Беларусь</option><option value=\'39\'  >Болгария</option><option value=\'63\'  >Великобритания</option><option value=\'68\'  >Венгрия</option><option value=\'134\'  >Вьетнам</option><option value=\'61\'  >Германия</option><option value=\'454\'  >Гонконг</option><option value=\'372\'  >Греция</option><option value=\'60\'  >Грузия</option><option value=\'54\'  >Дания</option><option value=\'321\'  >Доминикана</option><option value=\'338\' selected="selected" >Египет</option><option value=\'75\'  >Израиль</option><option value=\'69\'  >Индия</option><option value=\'330\'  >Индонезия</option><option value=\'323\'  >Иордания</option><option value=\'320\'  >Испания</option><option value=\'76\'  >Италия</option><option value=\'80\'  >Кения</option><option value=\'376\'  >Кипр</option><option value=\'46\'  >Китай</option><option value=\'49\'  >Коста-Рика</option><option value=\'9\'  >Куба</option><option value=\'7\'  >Латвия</option><option value=\'10\'  >Литва</option><option value=\'90\'  >Маврикий</option><option value=\'88\'  >Малайзия</option><option value=\'324\'  >Мальдивы</option><option value=\'414\'  >Мальта</option><option value=\'97\'  >Марокко</option><option value=\'91\'  >Мексика</option><option value=\'98\'  >Нидерланды</option><option value=\'103\'  >Норвегия</option><option value=\'16\'  >ОАЭ</option><option value=\'2219\'  >Оман</option><option value=\'109\'  >Польша</option><option value=\'110\'  >Португалия</option><option value=\'112\'  >Румыния</option><option value=\'1016\'  >Сейшельские о.</option><option value=\'116\'  >Сингапур</option><option value=\'117\'  >Словакия</option><option value=\'118\'  >Словения</option><option value=\'131\'  >США</option><option value=\'332\'  >Таиланд</option><option value=\'1082\'  >Танзания</option><option value=\'378\'  >Тунис</option><option value=\'318\'  >Турция</option><option value=\'6\'  >Украина</option><option value=\'108\'  >Филиппины</option><option value=\'354\'  >Финляндия</option><option value=\'420\'  >Франция</option><option value=\'442\'  >Хорватия</option><option value=\'434\'  >Черногория</option><option value=\'53\'  >Чехия</option><option value=\'122\'  >Швейцария</option><option value=\'121\'  >Швеция</option><option value=\'334\'  >Шри Ланка</option><option value=\'56\'  >Эстония</option><option value=\'119\'  >Южная Корея</option><option value=\'1086\'  >Ямайка</option>              </select>';
tour_search_module += '</li>';
tour_search_module += '<li>';
tour_search_module += '<label>Регион:</label>';
tour_search_module += '<select id="region_list" name="region_list" class="regions" multiple="multiple">';
tour_search_module += '<option value=\'0\' selected="selected" >Все регионы</option><option value=\'5486\'  >Дахаб</option><option value=\'2\'  >Макади Бей</option><option value=\'5478\'  >Марса Алам</option><option value=\'5409\'  >Сафага</option><option value=\'5522\'  >Сахл Хашиш</option><option value=\'3\'  >Сома Бей</option><option value=\'5185\'  >Таба</option><option value=\'1\'  >Хургада</option><option value=\'5996\'  >Шарм Эль Шейх</option><option value=\'5476\'  >Эль Гуна</option><option value=\'5509\'  >Эль Кусейр</option>              </select>';
tour_search_module += '<img id="ajax_loader_region" class="ajax_loader" src="https://www.ittour.com.ua/classes/handlers/ittour_external_modules/ittour_modules/images/ajax_loader.gif"  alt="loading..."  />';
tour_search_module += '</li>';
tour_search_module += '</ul>';
tour_search_module += '</div>';
tour_search_module += '<div class="hotel">';
tour_search_module += '<label>Отель:</label>';
tour_search_module += '<ul class="star" id="itt_hotel_rating_block">';
tour_search_module += '<li><input class=\'hotel_rating\' name=\'hotel_rating_list\' value=\'7\' type=\'checkbox\'  /><span class=\'nbre_hotel_rating\'>2*</span></li><li><input class=\'hotel_rating\' name=\'hotel_rating_list\' value=\'3\' type=\'checkbox\'  /><span class=\'nbre_hotel_rating\'>3*</span></li><li><input class=\'hotel_rating\' name=\'hotel_rating_list\' value=\'4\' type=\'checkbox\' checked="checked" /><span class=\'nbre_hotel_rating\'>4*</span></li><li><input class=\'hotel_rating\' name=\'hotel_rating_list\' value=\'78\' type=\'checkbox\' checked="checked" /><span class=\'nbre_hotel_rating\'>5*</span></li>          </ul>';
tour_search_module += '<select id="hotel_list" name="hotel_list" multiple="multiple" class="hotel-list">';
tour_search_module += '<option value=\'0\' selected="selected" >Все отели</option><option value=\'23\'  >AA Grand Oasis Resort (ex.Tropicana Grand Oasis Resort)</option><option value=\'81386\'  >Akassia Club Calimera Swiss Resort</option><option value=\'9240\'  >Al Mas Red Sea Palace (ex.Golden Five Al Mas Palace)</option><option value=\'2976\'  >Al Nabila Grand Bay Makadi</option><option value=\'2984\'  >Albatros Aqua Blu Resort Sharm El Sheikh</option><option value=\'3995\'  >Albatros Aqua Park (ex.Albatros Garden Resort)</option><option value=\'2972\'  >Albatros Aqua Vista Resort & Spa</option><option value=\'1339772\'  >Albatros Aquapark Sharm El Sheikh</option><option value=\'2974\'  >Albatros Palace Hotel Resort & SPA</option><option value=\'811\'  >Albatros White Beach (ex. Royal Palace)</option><option value=\'2975\'  >Alf Leila Wa Leila</option><option value=\'4127\'  >Ali Baba Palace</option><option value=\'306397\'  >Amazonia Gardenia Hurghada (ex.Gardenia Plaza Resort)</option><option value=\'36488\'  >Amc Royal (Ex Amc Azur)</option><option value=\'21414\'  >Amwaj Blue Beach Resort & SPA</option><option value=\'20\'  >Amwaj Oyoun Hotel & Resort (Ex. Amwaj Hotel & Resort)</option><option value=\'1279149\'  >Ancient Sands Golf Resort & Residences</option><option value=\'23730\'  >Aqua Hotel Resort & SPA (ex. Top Choice Sharm Bride)</option><option value=\'2884\'  >Arabella Azur Resort</option><option value=\'7534\'  >Arabia Azur Beach Resort</option><option value=\'6355\'  >Aurora Bay Resort Marsa Alam (ex.Oriental Bay Resort)</option><option value=\'590\'  >Aurora Cyrene Hotel (ex.Crystal Cyrene)</option><option value=\'1421\'  >Aurora Nada Resort (Ex-Nada Resort)</option><option value=\'6356\'  >Aurora Oriental Resort Sharm El Sheikh (Ex-Oriental Resort Sharm El Sheikh)</option><option value=\'164\'  >Aurora Sharm Hotel (ex.Crystal Sharm)</option><option value=\'605\'  >Azure Club Resort</option><option value=\'14272\'  >Barcelo Tiran Beach Sharm (Ex-Tiran Sharm)</option><option value=\'634235\'  >Baron Palace Resort Sahl Hasheesh</option><option value=\'1329\'  >Baron Palms</option><option value=\'978\'  >Baron Resort</option><option value=\'3006\'  >Beach Albatros Resort Hurghada</option><option value=\'642273\'  >Beach Color Smart Line Resort</option><option value=\'11054\'  >Bel Air Azur Resort</option><option value=\'9586\'  >Bella Vista Resort</option><option value=\'1334166\'  >Bellevue Beach Hotel (El Gouna)</option><option value=\'48784\'  >Blue Reef Resort</option><option value=\'2814\'  >Caribbean World Resort</option><option value=\'828788\'  >Caves Beach Resort</option><option value=\'14716\'  >Citadel Azur Resort</option><option value=\'600174\'  >Cleopatra Luxury Resort Makadi Bay</option><option value=\'35534\'  >Cleopatra Luxury Resort Sharm El Sheikh</option><option value=\'1984\'  >Club El Faraana Reef</option><option value=\'4707\'  >Club Reef</option><option value=\'9655\'  >Concorde El Salam Front</option><option value=\'9656\'  >Concorde El Salam Sport</option><option value=\'321246\'  >Concorde Moreen Beach Resort & Spa</option><option value=\'13746\'  >Continental Garden Reef Resort</option><option value=\'9371\'  >Continental Plaza Beach</option><option value=\'1569\'  >Coral Beach Montazah Resort (Ex.Coral Beach Rotana Resort Montazah)</option><option value=\'4\'  >Coral Beach Resort (Ex. Coral Beach Rotana Resort)</option><option value=\'49\'  >Coral Beach Tiran Resort (ex.Coral Beach Rotana Resort Tiran)</option><option value=\'311808\'  >Coral Hills Marsa Alam</option><option value=\'42106\'  >Coral Sea Holiday Village Hotel</option><option value=\'894\'  >Coral Sea Sensatori</option><option value=\'757\'  >Coral Sea Water World (ex. Coral Sea Resort)</option><option value=\'643916\'  >Coral Sun Beach Safaga</option><option value=\'30082\'  >Cyrene Grand Hotel (Ex. Melia Sharm)</option><option value=\'3009\'  >Dana Beach Resort</option><option value=\'713\'  >Delta Sharm</option><option value=\'1429\'  >Desert Light Solitaire Resort Marsa Alam</option><option value=\'672\'  >Desert Rose</option><option value=\'4097\'  >Dessole Aladdin Beach Resort (ex.Aladdin Beach Resort)</option><option value=\'176\'  >Dessole Cataract Layalina</option><option value=\'865\'  >Dessole Marlin Inn</option><option value=\'5964\'  >Dessole Pyramisa Sahl Hasheesh (Ex.Pyramisa Sahl Hasheesh)</option><option value=\'758\'  >Dive Inn</option><option value=\'1303116\'  >Domina Coral Bay Aquamarine</option><option value=\'13758\'  >Domina Coral Bay Aquamarine Beach</option><option value=\'913\'  >Domina Coral Bay Aquamarine Pool</option><option value=\'673\'  >Domina Coral Bay Elisir</option><option value=\'36\'  >Domina Coral Bay Harem</option><option value=\'1086126\'  >Domina Coral Bay Harem Deluxe</option><option value=\'667\'  >Domina Coral Bay Harem Duplex</option><option value=\'4862\'  >Domina Coral Bay King\'s Lake</option><option value=\'912\'  >Domina Coral Bay Oasis Garden</option><option value=\'907\'  >Domina Coral Bay Prestige</option><option value=\'675\'  >Domina Coral Bay Prestige Pool</option><option value=\'678\'  >Domina Coral Bay Prestige Sea</option><option value=\'1897\'  >Dreams Beach Resort</option><option value=\'4877\'  >Dreams Beach Resort Marsa Alam</option><option value=\'874\'  >Dreams Vacation Resort</option><option value=\'1162\'  >Ecotel Dahab Resort (Ex.Sol Dahab Resort)</option><option value=\'878206\'  >El Malikia Resort Abu Dabbab</option><option value=\'578578\'  >El Malikia Swiss Inn Resort (ex Sol Y Mar Abu Dabbab)</option><option value=\'1122\'  >El Wekala Golf Resort Taba Heights (ex.The Three Corners El Wekala Golf Resort)</option><option value=\'4920\'  >Elphistone Resort</option><option value=\'20536\'  >Elysees Hurgada Hotel</option><option value=\'927185\'  >Elysees Premier Hotel</option><option value=\'5046\'  >Fantazia Marsa Alam (Ex. Shores Fantazia)</option><option value=\'15522\'  >Faraana Heights Resort</option><option value=\'1127\'  >Flamenco Beach & Resort (El Quseir)</option><option value=\'1048659\'  >Flamenco Resort (El-Quseir)</option><option value=\'332686\'  >Fort Arabesque Resort</option><option value=\'622449\'  >Fortuna (El Gouna)</option><option value=\'752899\'  >Fortuna (El Gouna)</option><option value=\'2597\'  >Fortuna (Hrg)</option><option value=\'5002\'  >Fortuna (Hrg)</option><option value=\'748020\'  >Fortuna (Hurghada) First Line Only</option><option value=\'1194087\'  >Fortuna (Makadi)</option><option value=\'795277\'  >Fortuna (Makady)</option><option value=\'5003\'  >Fortuna (Marsa Alam)</option><option value=\'5004\'  >Fortuna (Marsa Alam)</option><option value=\'752892\'  >Fortuna (Safaga)</option><option value=\'14966\'  >Fortuna (Sharm)</option><option value=\'14970\'  >Fortuna (Sharm)</option><option value=\'1241683\'  >Fortuna (Sharm) 4+*/</option><option value=\'749500\'  >Fortuna (Sharm) First Line Only</option><option value=\'735013\'  >Fortuna (Soma Bay)</option><option value=\'1241510\'  >Fortuna (Taba)</option><option value=\'1333462\'  >Fortuna 5* Aqua Park Beach Resort (Sahl Hasheesh)</option><option value=\'1067477\'  >Fortuna Aqua Park (Makadi Bay)</option><option value=\'1062774\'  >Fortuna Aqua Park Beach Resort Hrg</option><option value=\'1304658\'  >Fortuna Aqua Park Resort Hrg</option><option value=\'205465\'  >Fortuna Egypt</option><option value=\'205466\'  >Fortuna Egypt</option><option value=\'5000\'  >Fortuna Plus (Hrg)</option><option value=\'10291\'  >Four Seasons Resort</option><option value=\'2254\'  >Gardenia Plaza Resort & Aqua Park (ex. Gardenia Plaza Hotel & Resort)</option><option value=\'13782\'  >Ghazala Beach</option><option value=\'13784\'  >Ghazala Gardens</option><option value=\'325\'  >Golden Five Club</option><option value=\'3837\'  >Golden Five Diamond Hotel</option><option value=\'3835\'  >Golden Five Emerald</option><option value=\'1269\'  >Golden Five Sapphire Suites Hotel</option><option value=\'1267\'  >Golden Five Topaz Suites Club Hotel</option><option value=\'3998\'  >Grand Hotel</option><option value=\'30434\'  >Grand Hotel Sharm</option><option value=\'5231\'  >Grand Makadi</option><option value=\'11329\'  >Grand Plaza Hotel</option><option value=\'686\'  >Grand Plaza Resort Hurghada</option><option value=\'8405\'  >Grand Plaza Resort Sharm</option><option value=\'3016\'  >Grand Resort Hurghada</option><option value=\'5253\'  >Grand Rotana Resort & Spa</option><option value=\'1384\'  >Grand Seas Resort Hostmark</option><option value=\'11612\'  >Harmony Makadi Bay Hotel & Resort</option><option value=\'1992\'  >Hawaii Le Jardin Aqua Park (ex.Festival Le Jardin)</option><option value=\'1251222\'  >Hawaii Riviera Club</option><option value=\'196\'  >Hawaii Riviera Resort & Aqua Park (Ex Festival Riviera Resort)</option><option value=\'867\'  >Helnan Marina</option><option value=\'24632\'  >Hilton Fayrouz Resort</option><option value=\'9379\'  >Hilton Hurghada Long Beach Resort</option><option value=\'343\'  >Hilton Hurghada Plaza</option><option value=\'13\'  >Hilton Hurghada Resort</option><option value=\'172802\'  >Hilton Marsa Alam Nubian Resort</option><option value=\'430273\'  >Hilton Sharks Bay</option><option value=\'13794\'  >Hilton Sharm Dreams Resort</option><option value=\'5378\'  >Hilton Taba Resort</option><option value=\'93\'  >Hilton Waterfalls Resort</option><option value=\'47\'  >Hyatt Regency</option><option value=\'9380\'  >Iberotel Makadi Beach</option><option value=\'1910\'  >Iberotel Palace</option><option value=\'738\'  >Ibis Styles Dahab Lagoon (ex.Coralia Club Dahab)</option><option value=\'42026\'  >Il Mercato Hotel (ex.Iberotel Il Mercato)</option><option value=\'4014\'  >Imperial Shams Abu Soma</option><option value=\'13880\'  >Island Garden Resort (Ex Sunrise Island Garden)</option><option value=\'812\'  >Island View Resort</option><option value=\'758497\'  >Jasmine Palace Resort</option><option value=\'975\'  >Jaz Aquamarine (ex.Iberotel Aquamarine)</option><option value=\'944699\'  >Jaz Aquaviva Makadi Bay</option><option value=\'10368\'  >Jaz Belvedere</option><option value=\'324188\'  >Jaz Blue Marine</option><option value=\'5591\'  >Jaz Dahabeya (Ex Iberotel Dahabeya)</option><option value=\'1027\'  >Jaz Dar El Madina (ex Sol Y Mar Dar El Madina)</option><option value=\'5592\'  >Jaz Fanara Resort & Residence (ex.Iberotel Club Fanara)</option><option value=\'1026\'  >Jaz Lamaya (ex.Iberotel Lamaya Resort)</option><option value=\'880\'  >Jaz Lido (Ex. Iberotel Lido)</option><option value=\'16648\'  >Jaz Makadi Oasis (ex.Iberotel Makadi Oasis)</option><option value=\'309299\'  >Jaz Makadi Saraya Palms (ex.Iberotel Makadi Saraya Palms)</option><option value=\'9561\'  >Jaz Makadi Saraya Resort (ex.Iberotel Makadi Saraya Resort)</option><option value=\'9288\'  >Jaz Makadi Star</option><option value=\'9255\'  >Jaz Makadina (Ex. Sol Y Mar Club Makadi)</option><option value=\'17064\'  >Jaz Mirabel Beach</option><option value=\'77\'  >Jaz Mirabel Club Resort</option><option value=\'783\'  >Jaz Mirabel Park Resort</option><option value=\'863\'  >Jaz Samaya Resort (Ex.Iberotel Samaya Resort)</option><option value=\'688\'  >Jaz Solaya Resort (Ex Sol Y Mar Solaya)</option><option value=\'2973\'  >Jungle Aqua Park</option><option value=\'11011\'  >Kempinski Hotel Soma Bay</option><option value=\'2876\'  >King Tut Aqua Park Beach Resort</option><option value=\'9575\'  >Labranda Club Makadi (Ex.Club Azur)</option><option value=\'773908\'  >Labranda Garden Makadi (ex.Makadi Garden Azur Resort)</option><option value=\'2004\'  >Labranda Royal Makadi (ex.Royal Azur)</option><option value=\'6834\'  >Labranda Tower Bay (ex.Sharm Club)</option><option value=\'330192\'  >Laguna Beach Resort Marsa Alam</option><option value=\'16484\'  >Le Meridien Dahab</option><option value=\'343366\'  >Le Mirage New Tower ( Ex Tower Bay Resort)</option><option value=\'957\'  >Le Pacha Resort</option><option value=\'9348\'  >Le Royale Sonesta Collection Luxury Resort</option><option value=\'8332\'  >Lilly Land</option><option value=\'5953\'  >Lotus Bay Safaga</option><option value=\'2798\'  >Lti Akassia Beach Marsa Alam</option><option value=\'1374\'  >Magawish Village & Resort (Ex-Magawish Swiss Inn Resort)</option><option value=\'9595\'  >Magic Life Kalawy Imperial</option><option value=\'242\'  >Magic Life Sharm El Sheikh Imperial</option><option value=\'5991\'  >Makadi Palace</option><option value=\'670333\'  >Makadi Spa</option><option value=\'1339\'  >Marina Lodge Port Ghalib Resort</option><option value=\'795148\'  >Marina View Port Ghalib</option><option value=\'695\'  >Maritim Jolie Ville Golf & Resort</option><option value=\'976\'  >Maritim Jolie Ville Resort & Casino</option><option value=\'706\'  >Maritim Jolie Ville Royal Peninsula Hotel & Resort</option><option value=\'967\'  >Marriott Hurghada</option><option value=\'1279404\'  >Mazar Resort & Spa</option><option value=\'60\'  >Melton Beach (ex. Melia Sinai)</option><option value=\'286\'  >Melton Tiran Resort (ex.Tiran Island)</option><option value=\'6113\'  >Menaville</option><option value=\'16260\'  >Mercure Hurghada (ex. Sofitel Hurghada)</option><option value=\'897\'  >Mexicana Sharm Resort</option><option value=\'6151\'  >Minamark</option><option value=\'813778\'  >Mirage Aqua Park & Spa</option><option value=\'323614\'  >Mirage New Hawaii Resort & Spa</option><option value=\'16254\'  >Miramar Resort Taba Heights (ex.Hyatt Regency Taba Heights) 5*</option><option value=\'23662\'  >Monte Carlo Sharm El Sheikh (Ex The Ritz-Carlton)</option><option value=\'3923\'  >Movenpick Hurghada Resort (ex.Continental Resort Hurghada)</option><option value=\'361\'  >Movenpick Resort & Spa El Gouna</option><option value=\'3771\'  >Movenpick Resort El Quseir</option><option value=\'10263\'  >Movenpick Resort Naama Bay (Ex - Sofitel Ssh)</option><option value=\'578633\'  >Movenpick Resort Soma Bay</option><option value=\'245\'  >Movenpick Resort Taba</option><option value=\'2817\'  >New Tiran Hotel (ex.Tropicana New Tiran)</option><option value=\'3797\'  >Noria Resort</option><option value=\'9496\'  >Novotel Beach Sharm El Sheikh</option><option value=\'1016213\'  >Novotel Marsa Alam</option><option value=\'708\'  >Novotel Palm</option><option value=\'841251\'  >Nubia Aqua Beach Resort</option><option value=\'765\'  >Nubian Island</option><option value=\'764\'  >Nubian Village</option><option value=\'11012\'  >Oberoi Sahl Hasheesh</option><option value=\'17926\'  >Old Palace Resort</option><option value=\'1344537\'  >Old Vic Sharm</option><option value=\'39090\'  >Onatti Beach Resort</option><option value=\'1911\'  >Oriental Rivoli</option><option value=\'1842\'  >Otium Hotel Aloha Sharm (Ex. Shores Aloha)</option><option value=\'31\'  >Otium Hotel Amphoras Sharm (ex.Shores Amphoras Resort)</option><option value=\'9254\'  >Palm Beach Resort</option><option value=\'1933\'  >Palm Royale Soma Bay</option><option value=\'6396\'  >Panorama Bungalows Hurghada</option><option value=\'6397\'  >Panorama Bungalows Resort El Gouna</option><option value=\'3838\'  >Paradise Red Sea (ex.Golden Five Paradise)</option><option value=\'51\'  >Park Inn By Radisson</option><option value=\'616\'  >Pasadena Hotel</option><option value=\'3994\'  >Pickalbatros Sea World (ex Aqua Blu)</option><option value=\'9499\'  >Poinciana Sharm Resort (Ex Grand Sharm Resort)</option><option value=\'681\'  >Port Ghalib Resort (ex. Crowne Plaza Sahara Oasis)</option><option value=\'973\'  >Premier Romance</option><option value=\'332616\'  >Premium Grand Horizon Hurghada (ex. Montillon Grand Horizon Resort)</option><option value=\'13708\'  >Prima Life Makadi</option><option value=\'8336\'  >Pyramisa Sharm El Sheikh Resort (ex.Dessole Pyramisa)</option><option value=\'1004627\'  >Radisson Blu Lagoon Sharm El Sheikh</option><option value=\'17\'  >Radisson Blu Resort</option><option value=\'10125\'  >Radisson Blu Resort El Quseir</option><option value=\'124\'  >Reef Oasis Beach Resort</option><option value=\'2897\'  >Reef Oasis Blue Bay Resort & Spa</option><option value=\'565\'  >Regency Plaza Aqua Park & Spa</option><option value=\'322\'  >Regina Swiss Inn Resort (ex.Regina Aqua Park Beach Resort)</option><option value=\'322202\'  >Rehana Prestige Resort & Spa</option><option value=\'9517\'  >Rehana Royal Beach Resort & SPA</option><option value=\'38750\'  >Rehana Royal Prestige</option><option value=\'2920\'  >Rehana Sharm Resort</option><option value=\'697\'  >Renaissance Golden View Beach</option><option value=\'949561\'  >Resta Club Port Ghalib</option><option value=\'1379\'  >Resta Grand Resort</option><option value=\'1365\'  >Resta Reef Resort</option><option value=\'2792\'  >Riviera Plaza Abu Soma (ex.Lamar Resort Abu Soma)</option><option value=\'923220\'  >Rixos Seagate Sharm</option><option value=\'9520\'  >Rixos Sharm El Sheikh Resort (Ex Royal Grand Azur)</option><option value=\'6681\'  >Royal Albatros Moderna</option><option value=\'712\'  >Royal Grand Sharm</option><option value=\'6549\'  >Royal Lagoons Resort (ex.Premium Blue lagoon)</option><option value=\'923820\'  >Royal Monte Carlo Sharm El Sheikh</option><option value=\'501\'  >Royal Paradise Resort</option><option value=\'807\'  >Royal Savoy</option><option value=\'785331\'  >Royal Savoy Suites & Villas</option><option value=\'1165816\'  >Royal Tulip Beach Resort</option><option value=\'5359\'  >Safir Dahab Resort (Ex.Dahab Resort)</option><option value=\'1280115\'  >Samra Bay Hotel & Resort</option><option value=\'1064334\'  >Savoy Diplmatic Suite</option><option value=\'10070\'  >Savoy Hotel</option><option value=\'1050262\'  >Savoy Villas</option><option value=\'626\'  >Sea Beach Resort & Aqua Park (ex.Tropicana Sea Beach)</option><option value=\'304298\'  >Sea Club Aqua Park Sharm</option><option value=\'6780\'  >Sea Club Resort</option><option value=\'6782\'  >Sea Garden</option><option value=\'368\'  >Sea Gull Beach Resort</option><option value=\'1916\'  >Sea Life</option><option value=\'6791\'  >Sea Star Beau Rivage</option><option value=\'6792\'  >Sea Sun Dahab</option><option value=\'931\'  >Sensimar Premier Le Reve Hotel & Spa (ex. Premier Le Reve)</option><option value=\'6817\'  >Sentido Oriental Dreams Resort</option><option value=\'146618\'  >Sentido Reef Oasis Senses Resort (еx.Reef Oasis Senses Resort)</option><option value=\'182254\'  >Serenity Fun City Makadi Bay</option><option value=\'364\'  >Serenity Makadi Beach</option><option value=\'1420\'  >Shams Alam Beach Resort</option><option value=\'2787\'  >Shams Safaga</option><option value=\'232\'  >Sharm Cliff Resort</option><option value=\'104978\'  >Sharm El Sheikh Marriott Resort - Beach Side</option><option value=\'103476\'  >Sharm El Sheikh Marriott Resort - Mountain Side</option><option value=\'3816\'  >Sharm Holiday Resort</option><option value=\'26632\'  >Sharm Inn Amarein</option><option value=\'6837\'  >Sharm Plaza (ex.Crowne Plaza Resort)</option><option value=\'6839\'  >Sharm Resort</option><option value=\'50\'  >Sharming Inn</option><option value=\'9598\'  >Sheraton Miramar</option><option value=\'10262\'  >Sheraton Sharm Main Building</option><option value=\'1332\'  >Sheraton Soma Bay Resort</option><option value=\'587\'  >Sierra</option><option value=\'24126\'  >Sindbad Aqua Hotel</option><option value=\'1068399\'  >Sindbad Aqua Park</option><option value=\'1316839\'  >Sindbad Club Aqua Hotel & Spa</option><option value=\'1249977\'  >Sindbad Club Aqua Resort</option><option value=\'6888\'  >Siva Grand Beach</option><option value=\'1340\'  >Siva Port Ghalib (EX. Crown Plaza Sahara Sands Resort)</option><option value=\'6772\'  >Siva Sharm (Ex.Savita Resort)</option><option value=\'1117\'  >Sofitel Taba Heights</option><option value=\'1137\'  >Sol Taba Red Sea Resort (Ex.Sonesta Beach Resort Taba)</option><option value=\'9242\'  >Sol Y Mar Makadi Sun</option><option value=\'24490\'  >Sol Y Mar Naama Bay</option><option value=\'396\'  >Sol Y Mar Paradise</option><option value=\'1902\'  >Sol Y Mar Sharks Bay</option><option value=\'704\'  >Sonesta Beach</option><option value=\'832\'  >Sonesta Pharaoh Beach Resort</option><option value=\'932\'  >Sphinx Aqua Park Beach Resort</option><option value=\'331\'  >Steigenberger Al Dau Beach</option><option value=\'1034509\'  >Steigenberger Alcazar</option><option value=\'634236\'  >Steigenberger Aqua Magic</option><option value=\'8501\'  >Steigenberger Coraya Beach Resort (ex.Iberotel Coraya Beach Resort)</option><option value=\'9588\'  >Steigenberger Golf Resort El Gouna</option><option value=\'786\'  >Stella Di Mare Beach Hotel & SPA</option><option value=\'7028\'  >Stella Makadi Beach Resort</option><option value=\'1157\'  >Strand Taba Heights Beach & Golf Resort</option><option value=\'1920\'  >Sultan Gardens Resort</option><option value=\'14\'  >Sunny Days El Palacio</option><option value=\'852\'  >Sunny Days Palma De Mirette</option><option value=\'3778\'  >Sunrise Diamond Beach</option><option value=\'308347\'  >Sunrise Grand Select Arabian Beach Resort</option><option value=\'7072\'  >Sunrise Grand Select Crystal Bay Resort</option><option value=\'752695\'  >Sunrise Grand Select Montemare</option><option value=\'22802\'  >Sunrise Holidays</option><option value=\'1502\'  >Sunrise Mamlouk Palace</option><option value=\'918974\'  >Sunrise Marina Port Ghalib (ex.Rehana Royal Port Ghalib)</option><option value=\'2748\'  >Sunrise Royal Makadi Resort</option><option value=\'2906\'  >Sunrise Select Garden Beach Resort (ex.Sunrise Garden Beach)</option><option value=\'757437\'  >Sunwing Family Star Makadi</option><option value=\'670334\'  >Sunwing Waterworld Makadi</option><option value=\'16660\'  >Swiss Inn Dreams Resort</option><option value=\'988\'  >Swiss Inn Resort Dahab</option><option value=\'94492\'  >Tamra Beach</option><option value=\'275492\'  >Tez Deluxe Variant Hurghada</option><option value=\'187733\'  >Tez Deluxe Variant Sharm</option><option value=\'188692\'  >Tez Variant Hurghada</option><option value=\'188693\'  >Tez Variant Hurghada</option><option value=\'9200\'  >Tez Variant Sharm</option><option value=\'9201\'  >Tez Variant Sharm</option><option value=\'9378\'  >The Movie Gate Hurghada (ex.Club Calimera Hurghada)</option><option value=\'684\'  >The Palace Port Ghalib (ex.Intercontinental The Palace Port Ghalib)</option><option value=\'870258\'  >The Three Corners Equinox Beach Resort</option><option value=\'1370\'  >The Three Corners Fayrouz Plaza</option><option value=\'273279\'  >The Three Corners Happy Life Beach Resort</option><option value=\'1347\'  >The Three Corners Ocean View El Gouna</option><option value=\'617\'  >The Three Corners Palmyra</option><option value=\'21402\'  >The Three Corners Pensee Beach Resort (Ex Pensee Royal Garden Resort)</option><option value=\'7863\'  >The Three Corners Rihana Inn</option><option value=\'341\'  >The Three Corners Rihana Resort</option><option value=\'705903\'  >The Three Corners Royal Star Beach Resort</option><option value=\'265773\'  >The Three Corners Sea Beach Resort</option><option value=\'824791\'  >The Three Corners Sea Equinox Beach Resort</option><option value=\'862\'  >The Three Corners Sunny Beach Resort</option><option value=\'320\'  >Tia Heights Makadi Bay</option><option value=\'331554\'  >Tia Heights Makadi Bay Aquapark Resort</option><option value=\'843\'  >Tirana Aqua Park (Ex-Sunrise Tirana Aqua Park)</option><option value=\'1352\'  >Titanic Beach SPA & Aqua Park</option><option value=\'386\'  >Titanic Palace Resort</option><option value=\'7252\'  >Titanic Resort (ex.Titanic Resort and Aqua Park)</option><option value=\'840124\'  >Tolip Taba Resort & Spa</option><option value=\'1058\'  >Tropitel Dahab Oasis</option><option value=\'1924\'  >Tropitel Naama Bay</option><option value=\'183943\'  >Tropitel Sahl Hasheesh</option><option value=\'7301\'  >Tulip Marsa Alam</option><option value=\'774\'  >Turquoise Beach Hotel</option><option value=\'2790\'  >Utopia Beach Club</option><option value=\'838618\'  >Vera Club Queen Beach Sharm</option><option value=\'835579\'  >Vera Club Queen View Sharm</option><option value=\'682\'  >Wadi Lahmy Azur Resort</option><option value=\'1017716\'  >Westin Soma Bay (ex.La Residence Des Cascades)</option><option value=\'236\'  >Xperience Kiroseiz Parkland (Ex. The Three Corners Kiroseiz)</option><option value=\'954579\'  >Xperience Kirosieiz Premier</option><option value=\'183947\'  >Xperience Sea Breeze Resort</option><option value=\'922\'  >Xperience St George Homestay (Ex.St George Three Corners Resort)</option><option value=\'674\'  >Zee Brayka Bay Resort (ex.Brayka Bay Resort)</option><option value=\'192207\'  >Zee Royal Brayka Beach Resort (ex.Braykabay Royal)</option>          </select>';
tour_search_module += '<img id="ajax_loader_hotel" class="ajax_loader" src="https://www.ittour.com.ua/classes/handlers/ittour_external_modules/ittour_modules/images/ajax_loader.gif"  alt="loading..."  />';
tour_search_module += '</div>';
tour_search_module += '<div class="fly-food">';
tour_search_module += '<div class="food_frame">';
tour_search_module += '<label class="food-title">Питание:</label>';
tour_search_module += '<select name="food" class="itt_nutrition_select" >';
tour_search_module += '<option value="496 388 498 512 560 1956">Любое питание</option>';
tour_search_module += '<option value="388 496 498 512 560">Завтрак и лучше</option>';
tour_search_module += '<option value="496 498 512 560">Полупансион и лучше</option>';
tour_search_module += '<option value="498 512 560">Полный пансион и лучше</option>';
tour_search_module += '<option value="498 512 560" selected="selected">Все включено и лучше</option>';
tour_search_module += '<option value="560">Ультра всё включено</option>';
tour_search_module += '</select>';
tour_search_module += '</div>';
tour_search_module += '<div class="title_parent_child"><label class="title_parent_child">Размещение в номере:</label></div>';
tour_search_module += '<div class="parent-child">';
tour_search_module += '<label class="parent">Взрослые:</label>';
tour_search_module += '<select class="small" name="adults" id="adult">';
tour_search_module += '<option value=\'1\'  >1</option><option value=\'2\' selected="selected" >2</option><option value=\'3\'  >3</option><option value=\'4\'  >4</option>            </select>';
tour_search_module += '<label class="child">Дети:</label>';
tour_search_module += '<select class="small" id="children" name="children">';
tour_search_module += '<option value=\'0\' selected="selected" >0</option><option value=\'1\'  >1</option><option value=\'2\'  >2</option><option value=\'3\'  >3</option>            </select>';
tour_search_module += '</div>';
tour_search_module += '<div class="child_age-select children_age" style=\'display: none\' >';
tour_search_module += '<label class="title_children_age">Возраст детей:</label>';
tour_search_module += '<input  type="text" value="8" id="child1_age" name="child1_age"';
tour_search_module += 'class="age_small input_children_age" style=\'display: none\' disabled=\'disabled\' />';
tour_search_module += '<input type="text" value="8" id="child2_age" name="child2_age"';
tour_search_module += 'class="age_small input_children_age" style=\'display: none\' disabled=\'disabled\' />';
tour_search_module += '<input type="text" value="8" id="child3_age" name="child3_age"';
tour_search_module += 'class="age_small input_children_age" style=\'display: none\' disabled=\'disabled\' />';
tour_search_module += '</div>';
tour_search_module += '</div>';
tour_search_module += '</div>';
tour_search_module += '</div>';
tour_search_module += '<div class="second_box">';
tour_search_module += '<div class="col-detail-type">';
tour_search_module += '<div class="fly-date">';
tour_search_module += '<label>Дата вылета:</label>';
tour_search_module += '<div class="date-select">';
tour_search_module += '<span class="txt">c</span>';
tour_search_module += '<input autocomplete="off" type="text" class="small" name="date_from" id="itt_date_from" value="15.03.17" style="height: 20px!important; line-height: 16px!important;padding: 0 0 0 2px!important;" />';
tour_search_module += '<span class="txt">по</span>';
tour_search_module += '<input autocomplete="off" type="text" class="small" name="date_till" id="date_till" value="27.03.17" style="height: 20px!important; line-height: 16px!important;padding: 0 0 0 2px!important;" />';
tour_search_module += '</div>';
tour_search_module += '</div>';
tour_search_module += '<div class="night-age">';
tour_search_module += '<label>Ночей в туре:</label>';
tour_search_module += '<div class="date-select">';
tour_search_module += '<span class="txt">от</span>';
tour_search_module += '<select class="small" id="night_from" name="night_from" style="max-height: 22px!important;">';
tour_search_module += '<option value=\'1\'  >1</option><option value=\'3\'  >3</option><option value=\'4\'  >4</option><option value=\'6\' selected="selected" >6</option><option value=\'7\'  >7</option><option value=\'8\'  >8</option><option value=\'10\'  >10</option><option value=\'12\'  >12</option><option value=\'14\'  >14</option>            </select>';
tour_search_module += '<span class="txt">до</span>';
tour_search_module += '<select class="small" id="night_till" name="night_till" style="max-height: 22px!important;">';
tour_search_module += '<option value=\'4\'  >4</option><option value=\'6\'  >6</option><option value=\'8\' selected="selected" >8</option><option value=\'10\'  >10</option><option value=\'12\'  >12</option><option value=\'14\'  >14</option><option value=\'16\'  >16</option><option value=\'18\'  >18</option><option value=\'21\'  >21</option>            </select>';
tour_search_module += '</div>';
tour_search_module += '</div>';
tour_search_module += '<div class="itt_price">';
tour_search_module += '<label>Цена за номер:</label>';
tour_search_module += '<div class="date-select">';
tour_search_module += '<span class="txt first">от</span>';
tour_search_module += '<input type="text" value="0" id="price_from" name="price_from"';
tour_search_module += 'class="small" style="height: 20px!important; line-height: 16px!important;padding: 0 0 0 2px!important;" />';
tour_search_module += '<span class="txt">до</span>';
tour_search_module += '<input type="text" value="900000" id="price_till" name="price_till"';
tour_search_module += 'class="small" style="height: 20px!important; line-height: 16px!important;padding: 0 0 0 2px!important;" />';
tour_search_module += '<select class="unit" name="switch_price" id="switch_price">';
tour_search_module += '<option value="UAH">Грн</option>';
tour_search_module += '<option value="USD" selected="selected">USD</option>';
tour_search_module += '<option value="EUR">EUR</option>';
tour_search_module += '</select>';
tour_search_module += '</div>';
tour_search_module += '</div>';
tour_search_module += '</div>';
tour_search_module += '<div class="fly_from_box">';
tour_search_module += '<div class="first_part">';
tour_search_module += '<label class="fly">Вылет из:</label>';
tour_search_module += '<select id="departure_city" name="departure_city" class="fly-from">';
tour_search_module += '<option value=\'30419\'  >Днепр</option><option value=\'1212\'  >Запорожье</option><option value=\'2014\' selected="selected" >Киев</option><option value=\'1745\'  >Львов</option><option value=\'449\'  >Одесса</option><option value=\'1225\'  >Харьков</option>          </select>';
tour_search_module += '</div>';
tour_search_module += '</div>';
tour_search_module += '<div class="pager-sub">';
tour_search_module += '<div class="btn-search">';
tour_search_module += '<input value="Найти" type="button" onclick="return package_search_form_submit(false);" />';
tour_search_module += '</div>';
tour_search_module += '<div class="logo_ittour"><a href="https://www.ittour.com.ua/" target="_blank" alt=""></a></div>';
tour_search_module += '<div style="clear:both;font-size:1px;height:1px;overflow:hidden;">&nbsp;</div>';
tour_search_module += '</div>';
tour_search_module += '</div>';
tour_search_module += '</div>';
tour_search_module += '</div>';
tour_search_module += '</form>';
tour_search_module += '</div>';
tour_search_module += '</div>';
tour_search_module += '<div class="extended_hike_search_form" style=\'display:none;\'>';
tour_search_module += '<div class="frame_block">';
tour_search_module += '<h1 class="title" style="display:none;" >Поиск тура</h1>';
tour_search_module += '<form method="get" id="hike_search_form"  name="search_form" action=\'hike_seach_result.php\'>';
tour_search_module += '<div class="itt_main_background">';
tour_search_module += '<input type="hidden" name=\'action\' value=\'hike_tour_search\' />';
tour_search_module += '<input type=\'hidden\' id=\'hike_country\' name=\'country\' />';
tour_search_module += '<input type=\'hidden\' id=\'transport\' name=\'transport\' />';
tour_search_module += '<input type=\'hidden\' id=\'city\' name=\'city\' />';
tour_search_module += '<input type=\'hidden\' id=\'tour_city\' name=\'tour_city\' />';
tour_search_module += '<div class="itt_links">';
tour_search_module += '<div class=\'package_tour no_active\' ><a href="javascript;" onclick="return display_package_form();">Пакетные</a></div>';
tour_search_module += '<div class=\'hike_tour active\'>Экскурсионные</div>';
tour_search_module += '<div class=\'check_option_tour\'>';
tour_search_module += '<input type="radio" value="1" name="hike_tour_type" id="hike_tour_type_1" checked="checked"><label for="hike_tour_type_1">Проезд включен</label>';
tour_search_module += '<input type="radio" value="0" name="hike_tour_type" id="hike_tour_type_2"><label for="hike_tour_type_2">Проезд не включен</label>';
tour_search_module += '</div>';
tour_search_module += '</div>';
tour_search_module += '<div class="itt_content">';
tour_search_module += '<div class="first_box">';
tour_search_module += '<div class="col-direction">';
tour_search_module += '<div class="country">';
tour_search_module += '<label>Страны:</label>';
tour_search_module += '<select onclick="country_list_click();" id="country_list" name="country_list" class="country_list" multiple="multiple">';
tour_search_module += '<option value=\'29\' selected="selected" >Австралия</option><option value=\'30\'  >Австрия</option><option value=\'31\'  >Азербайджан</option><option value=\'23\'  >Албания</option><option value=\'24\'  >Андорра</option><option value=\'26\'  >Аргентина</option><option value=\'27\'  >Армения</option><option value=\'2037\'  >Багамские Острова</option><option value=\'406\'  >Беларусь</option><option value=\'36\'  >Бельгия</option><option value=\'41\'  >Бирма</option><option value=\'39\'  >Болгария</option><option value=\'37\'  >Боливия</option><option value=\'38\'  >Босния и Герцеговина</option><option value=\'18\'  >Бразилия</option><option value=\'2247\'  >Бруней</option><option value=\'2245\'  >Бутан</option><option value=\'63\'  >Великобритания</option><option value=\'68\'  >Венгрия</option><option value=\'134\'  >Вьетнам</option><option value=\'61\'  >Германия</option><option value=\'372\'  >Греция</option><option value=\'60\'  >Грузия</option><option value=\'54\'  >Дания</option><option value=\'338\'  >Египет</option><option value=\'75\'  >Израиль</option><option value=\'69\'  >Индия</option><option value=\'330\'  >Индонезия</option><option value=\'323\'  >Иордания</option><option value=\'73\'  >Ирландия</option><option value=\'74\'  >Исландия</option><option value=\'320\'  >Испания</option><option value=\'76\'  >Италия</option><option value=\'1070\'  >Камбоджа</option><option value=\'43\'  >Канада</option><option value=\'80\'  >Кения</option><option value=\'376\'  >Кипр</option><option value=\'46\'  >Китай</option><option value=\'2351\'  >Корея</option><option value=\'9\'  >Куба</option><option value=\'1202\'  >Лаос</option><option value=\'7\'  >Латвия</option><option value=\'10\'  >Литва</option><option value=\'448\'  >Лихтенштейн</option><option value=\'86\'  >Люксембург</option><option value=\'1712\'  >Мадагаскар</option><option value=\'87\'  >Македония</option><option value=\'88\'  >Малайзия</option><option value=\'324\'  >Мальдивы</option><option value=\'414\'  >Мальта</option><option value=\'97\'  >Марокко</option><option value=\'91\'  >Мексика</option><option value=\'446\'  >Монако</option><option value=\'2646\'  >Мьянма</option><option value=\'99\'  >Непал</option><option value=\'98\'  >Нидерланды</option><option value=\'100\'  >Новая Зеландия</option><option value=\'103\'  >Норвегия</option><option value=\'107\'  >Перу</option><option value=\'109\'  >Польша</option><option value=\'110\'  >Португалия</option><option value=\'112\'  >Румыния</option><option value=\'444\'  >Сан-Марино</option><option value=\'115\'  >Сербия</option><option value=\'116\'  >Сингапур</option><option value=\'117\'  >Словакия</option><option value=\'118\'  >Словения</option><option value=\'131\'  >США</option><option value=\'332\'  >Таиланд</option><option value=\'1082\'  >Танзания</option><option value=\'2352\'  >Тибет</option><option value=\'318\'  >Турция</option><option value=\'6\'  >Украина</option><option value=\'1760\'  >Фиджи</option><option value=\'108\'  >Филиппины</option><option value=\'354\'  >Финляндия</option><option value=\'420\'  >Франция</option><option value=\'442\'  >Хорватия</option><option value=\'434\'  >Черногория</option><option value=\'53\'  >Чехия</option><option value=\'45\'  >Чили</option><option value=\'122\'  >Швейцария</option><option value=\'121\'  >Швеция</option><option value=\'848\'  >Шотландия</option><option value=\'334\'  >Шри Ланка</option><option value=\'55\'  >Эквадор</option><option value=\'56\'  >Эстония</option><option value=\'119\'  >Южная Корея</option><option value=\'1086\'  >Ямайка</option><option value=\'77\'  >Япония</option>            </select>';
tour_search_module += '</div>';
tour_search_module += '<div class="transport">';
tour_search_module += '<label>Транспорт:</label>';
tour_search_module += '<select onclick="transport_list_click();" id="transport_list" name="transport_list" multiple="multiple" class="transport-list">';
tour_search_module += '<option value=\'0\' selected="selected" >Все виды</option><option value=\'1\'  >Авиа</option>            </select>';
tour_search_module += '<img id="ajax_loader_transport" class="ajax_loader" src="https://www.ittour.com.ua/classes/handlers/ittour_external_modules/ittour_modules/images/ajax_loader.gif"  alt="loading..."  />';
tour_search_module += '</div>';
tour_search_module += '<div class="city">';
tour_search_module += '<label>Город отправления:</label>';
tour_search_module += '<select onclick="city_list_click();" id="city_list" name="city_list" class="city-list" multiple="multiple">';
tour_search_module += '<option value=\'0\' selected="selected" >Все города</option><option value=\'2014\'  >Киев</option>            </select>';
tour_search_module += '<img id="ajax_loader_city" class="ajax_loader" src="https://www.ittour.com.ua/classes/handlers/ittour_external_modules/ittour_modules/images/ajax_loader.gif"  alt="loading..."  />';
tour_search_module += '</div>';
tour_search_module += '<div class="tour_city">';
tour_search_module += '<label>Города на маршруте:</label>';
tour_search_module += '<select id="tour_city_list" name="tour_city_list" class="tour_city-list" multiple="multiple">';
tour_search_module += '<option value=\'0\' selected="selected" >Не имеет значения</option><option value=\'31746\'  >Аделаида</option><option value=\'8818\'  >Айерс Рок</option><option value=\'31748\'  >Алис Спрингс </option><option value=\'8812\'  >Брисбен</option><option value=\'49015\'  >Гамильтон</option><option value=\'4734\'  >Голд Кост</option><option value=\'22790\'  >Дарвин</option><option value=\'8808\'  >Квинсленд</option><option value=\'8814\'  >Кернс</option><option value=\'8810\'  >Куранда</option><option value=\'4730\'  >Мельбурн</option><option value=\'48771\'  >о. Кенгуру</option><option value=\'48770\'  >о. Филлип</option><option value=\'8816\'  >Палм Ков</option><option value=\'48551\'  >Ричмонд</option><option value=\'4732\'  >Сидней</option><option value=\'9883\'  >Тасмания</option><option value=\'48550\'  >Хобарт</option>            </select>';
tour_search_module += '<img id="ajax_loader_tour_city" class="ajax_loader" src="https://www.ittour.com.ua/classes/handlers/ittour_external_modules/ittour_modules/images/ajax_loader.gif"  alt="loading..."  />';
tour_search_module += '</div>';
tour_search_module += '</div>';
tour_search_module += '</div>';
tour_search_module += '<div class="second_box">';
tour_search_module += '<div class="pager-sub">';
tour_search_module += '<div class="day_price_box">';
tour_search_module += '<div class="first_part">';
tour_search_module += '<div class="first_part_title">';
tour_search_module += '<label class="date_to_start">Дата начала тура:</label>';
tour_search_module += '</div>';
tour_search_module += '<div class="date_from_to">';
tour_search_module += '<span class="txt first">c</span>';
tour_search_module += '<input type="text" class="small" name="hike_date_from" id="hike_date_from" value="14.03.17" />';
tour_search_module += '<span class="txt">по</span>';
tour_search_module += '<input type="text" class="small" name="hike_date_till" id="hike_date_till" value="14.05.17" />';
tour_search_module += '</div>';
tour_search_module += '</div>';
tour_search_module += '<div class="second_part">';
tour_search_module += '<div class="second_part_title">';
tour_search_module += '<label>Цена за 1-го взрослого:</label>';
tour_search_module += '</div>';
tour_search_module += '<div class="price_from_to">';
tour_search_module += '<span class="txt">до</span>';
tour_search_module += '<input type="text" class="small" name="hike_price_till" id="hike_price_till" value="500000" />';
tour_search_module += '<select class="unit" name="switch_price" id="switch_price">';
tour_search_module += '<option value="UAH">Грн</option>';
tour_search_module += '<option value="USD">USD</option>';
tour_search_module += '<option value="EUR" selected="selected">EUR</option>';
tour_search_module += '</select>';
tour_search_module += '</div>';
tour_search_module += '</div>';
tour_search_module += '<div class="third_part">';
tour_search_module += '</div>';
tour_search_module += '</div>';
tour_search_module += '<div class="pager_block">';
tour_search_module += '<div class="btn-search">';
tour_search_module += '<input value="Найти" type="button" onclick="return hike_search_form_submit(\'extended\', false);" />';
tour_search_module += '</div>';
tour_search_module += '<div class="logo_ittour" style="float: right !important; width: 80px !important; line-height: inherit !important; height: auto !important;';
tour_search_module += 'border: none !important; position: absolute !important; right: 118px !important;"><a href="https://www.ittour.com.ua/" style="width: 77px !important; height: 26px !important; display: block !important; text-decoration: none !important;';
tour_search_module += 'text-indent: -9999px !important; line-height: inherit !important; margin: 0 !important; padding: 0 !important;';
tour_search_module += 'border: none !important;" target="_blank" alt=""></a></div>';
tour_search_module += '<div style="clear:both;font-size:1px;height:1px;overflow:hidden;">&nbsp;</div>';
tour_search_module += '</div>';
tour_search_module += '</div>';
tour_search_module += '</div>';
tour_search_module += '</div>';
tour_search_module += '</div>';
tour_search_module += '</form>';
tour_search_module += '</div>';
tour_search_module += '</div>';
tour_search_module += '</div>';
tour_search_module += '<div class="clear"></div>';
tour_search_module += '</div>';
tour_search_module += '</td>';
tour_search_module += '</tr>';
tour_search_module += '<tr valign="top">';
tour_search_module += '<td align="center">';
tour_search_module += '<div class="itt_in_middel" style="margin:0 auto;width:100%;">';
tour_search_module += '<div class="itt_main_block" style="width:100% !important;">';
tour_search_module += '<div class="tour_load" style="width:100% !important;">';
tour_search_module += '<b>Мы ищем для вас самые лучшие варианты отдыха</b><br />';
tour_search_module += '<img src="https://www.ittour.com.ua/classes/handlers/ittour_external_modules/ittour_modules/images/ajax_loader.gif"><br />';
tour_search_module += 'Подождите пожалуйста!                </div>';
tour_search_module += '<div class="tour_search_result" style="width:100% !important;"></div>';
tour_search_module += '<div class="tour_order" style="display: none;" title=""></div>';
tour_search_module += '</div>';
tour_search_module += '<div class="clear"></div>';
tour_search_module += '</div>';
tour_search_module += '</td>';
tour_search_module += '</tr>';
tour_search_module += '</tbody>';
tour_search_module += '</table>';
tour_search_module += '</div>';
tour_search_module += '<div style="display: none;" id="search_results">Результаты поиска</div>';
tour_search_module += '<div style="display: none;" id="close">Закрыть</div>';
tour_search_module += '<div style="display: none;" id="tour_order_caption">Заявка на тур</div>';
tour_search_module += '<div style="display: none;" id="tour_search_result_caption"><strong>Результаты поиска туров</strong></div>';
tour_search_module += '<div style="display: none;" id="go_tour_search_caption"><strong>Мы ищем для вас самые лучшие варианты отдыха</strong></div>';
tour_search_module += '<div style="display: none;" id="please_wait_caption"><strong>Подождите пожалуйста!</strong></div>';
tour_search_module += '<div style="display: none;" id="show_info_caption"><strong>Показать информацию</strong></div>';
tour_search_module += '<div style="display: none;" id="hide_info_caption"><strong>Свернуть</strong></div>';
tour_search_module += '<div style="display: none;" class="itt_error_msg">По техническим причинам сервис временно недоступен. Приносим наши извинения.</div>';
document.getElementById('tour_search_module').innerHTML = tour_search_module;
